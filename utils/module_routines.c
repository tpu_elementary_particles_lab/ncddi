/* The MIT License (MIT)
 * 
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * */

# include <stdlib.h>
# include <stdio.h>
# include <string.h>
# include <assert.h>
# include "../module/ncddi_mod.h"

char testPhrase1[] = "Der Mensch ist ein Seil, geknupft zwischen Tier und Ubermensch.";
char testPhrase2[] = "All work an no play makes Jack a dull boy.";

int
main(int argc, char * argv[]) {
    struct RBuffer * rbf1,
                   * rbf2,
                   * rbf3;

    rbuf_init_pool();
    rbf1 = rbuf_obtain(strlen(testPhrase1)+1);
    rbf2 = rbuf_obtain(strlen(testPhrase2)+1);
    assert( rbf1->id != rbf2->id );
    strcpy( (char*) rbuf_data(rbf1), testPhrase1 );
    strcpy( (char*) rbuf_data(rbf2), testPhrase2 );

    assert( 0 == strcmp( testPhrase1, (char*) rbuf_data(rbf1) ) );
    assert( 0 == strcmp( testPhrase2, (char*) rbuf_data(rbf2) ) );
    assert( 0 == rbuf_reserve( rbf2, strlen(testPhrase2)+1 ) );

    strcpy( (char*) rbuf_data(rbf2), (char*) rbuf_data(rbf1) );
    assert( 0 == strcmp( testPhrase1, (char*) rbuf_data(rbf2) ) );

    rbf3 = rbuf_get_by_id( rbf1->id );
    assert( rbf3 == rbf1 );
    rbf3 = rbuf_get_by_id( rbf2->id );
    assert( rbf3 == rbf2 );

    for( BufferID i = 0; i < 3*KERNEL_IO_BUFFER_LENGTH; ++i ) {
        rbf3 = rbuf_obtain( KERNEL_IO_BUFFER_LENGTH + 1 );
        assert( rbf3 &&
            rbf3->id != rbf1->id &&
            rbf3->id != rbf2->id );
        memset( rbuf_data(rbf3), 'x', KERNEL_IO_BUFFER_LENGTH + 1 );
        rbuf_free( rbf3 );
    }

    rbuf_free( rbf1 );
    rbuf_free( rbf2 );

    rbuf_clear_pool();
    return EXIT_SUCCESS;
}

