/* The MIT License (MIT)
 * 
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * */

/**!@file ncddi_client.cpp
 *
 * A client daemon application that treats mimic devices.
 * 
 * Being invoked tries to read config file, submits
 * its PID to kernel module. If config parsed successfully
 * and kernel module being loading, becomes a daemon and
 * waits for signals from module:
 *  - SIG_SPAWN_CHILD (macro) causes app to invoke a child
 *    process that treats appeals to mimic device.
 *  - SIG_QUENCH (macro) causes termination of all childs.
 *
 *  Each treatment child is just can be thought as a
 *  micro-client that sends requests to the server via
 *  stream socket and (optionally) acquires data.
 */

# include <cstdlib>
# include <cstring>
# include <iostream>
# include <fcntl.h>
# include <cerrno>
# include <algorithm>
# if defined(__GXX_EXPERIMENTAL_CXX0X) || __cplusplus >= 201103L
#   include <unordered_set>
# else
#   include <set>
# endif
# include <sstream>  // XXX
# include <stropts.h>
# include <asm/ioctl.h>
# include "ncddi_app.hpp"
# include "ncddi_nw.hpp"
# include "ncddi_data_buffer.hpp"
# include "rapidxml-1.13/rapidxml.hpp"
# include "rapidxml-1.13/rapidxml_utils.hpp"

namespace ncddi {

static const char rootDevicePath[] = "/dev/" NCDDI_DEVICE_NAME "_root";
static const unsigned char mPassphrase[] = ROOT_PID_MSG_PASSPHRASE; 

class ReentrantConnection : protected Client {
private:
    DataBuffer _in, _out;
protected:
    std::string _srvHostName;
    int _portNo;
public:
    ReentrantConnection( const std::string & hostname, int portno ) :
            _srvHostName( hostname ), _portNo( portno ) {}
    ~ReentrantConnection() {}
    inline void open() { associate( _srvHostName, _portNo ); }
    inline bool is_associated() const { return Client::is_associated(); }
    inline void dispatch() {
        if( !Client::is_associated() ) { eraise( "Connection unassociated." ); }
        dispatch_request( _in, _out );
    }
    inline void close() { deassociate(); }

    inline const std::string & srv_hostname() const { return _srvHostName; }
    inline int portno() const { return _portNo; }

    inline DataBuffer & in_buf() {  return _in; }
    inline DataBuffer & out_buf() { return _out; }
};


/**
 * TODO: for each unique server we create a separate connection.
 * It has critical meaning for parallel treatment of several devices
 * with threads, but for now this mechanism is rudimentary.
 * */
class ClientApp : public App {
public:
    typedef App Parent;
    struct ImportDeviceEntry {
        std::string serverAddr;
        int serverPort;
        std::string path;
        std::string perm;
        mutable ReentrantConnection * _conn; // associated
        mutable DevMinorID _clientID, _hostID;
        // ...
    };
    /// Client's extension of App's CommonConfig.
    struct ClientConfig : public CommonConfig {
        std::vector<ImportDeviceEntry> imports;
        // ...
    };
    # if defined(__GXX_EXPERIMENTAL_CXX0X) || __cplusplus >= 201103L
    typedef std::unordered_set<ReentrantConnection *> ConnectionsSet;
    typedef std::unordered_map<DevMinorID, const ImportDeviceEntry *> DevicesDictionary;
    # else
    typedef std::set<ReentrantConnection *> ConnectionsSet;
    typedef std::map<DevMinorID, const ImportDeviceEntry *> DevicesDictionary;
    # endif
private:
    ClientConfig clientCfg; ///< Stores actual config instance.
    bool _pidSubmitted,     ///< Was PID submitted to module?
         _noNetwork;        ///< Is network disabled (DEVELOPMENT).
    /// Stores connections to device's hosts. Filled with _dispatch_import_entries().
    mutable ConnectionsSet _connections;
    /// A [client_device_id -> import_device_entry] dictionary.
    mutable DevicesDictionary _devDict;
protected:
    virtual void _V_configure( bool asDaemon ) _C11_override;
    virtual void _V_treat() _C11_override;
    virtual Thread * _V_spawn_worker(DevMinorID) _C11_override;
    /// Check in real device do exists and available at server.
    /// Creates new connection when needed.
    ReentrantConnection * _check_in_import_device( const ImportDeviceEntry & ) const;
    int _dispatch_import_entries() const;
public:
    ClientApp(bool noNetwork=false) :
                _pidSubmitted(false),
                _noNetwork(noNetwork) { _cCfgPtr = &clientCfg; }
    ~ClientApp();
    int communicate_with_module();

    static int send_meta_msg(const KMessage &);
    static std::string obtain_raw_device_name(const std::string &);
    static int obtain_device_permissions_code_from_str(const std::string &);
};

//
// STATIC ROUTINES
//

int
ClientApp::send_meta_msg( const KMessage & msg ) {
    int rc = 0,
        rootHubDevFile = open( "/dev/" NCDDI_DEVICE_NAME "_root", O_RDWR );
    if( rootHubDevFile <= 0 ) {
        nc_eprintf( "Couldn't open root device by path %s : %s.\n",
                 "/dev/" NCDDI_DEVICE_NAME "_root", strerror(errno));
        return rc = -1;
    }
    if( write( rootHubDevFile, &msg, sizeof(KMessage) ) ) {
        nc_eprintf( "Unable to write message to hub: %s.\n",
                 strerror(errno) );
        rc = -1;
    }
    close(rootHubDevFile);
    return rc;
}

static inline bool is_alphanum_nonspace(char c) {
    return ' ' == c || !std::isalnum(c);
}

std::string
ClientApp::obtain_raw_device_name(const std::string & pathStr) {
    std::string devName;
    if( 0 == pathStr.compare(0, 5, "/dev/") ) {
        devName = pathStr.substr(5, std::string::npos );
        if( devName.end() !=
            std::find_if( devName.begin(), devName.end(), is_alphanum_nonspace ) ) {
            nc_eprintf( "Can't use device name ``%s'' as it contains non alphanumeric characters.\n",
                devName.c_str());
            return "";
        }
    } else {
        nc_eprintf( "Device path not prefixed with '/dev/'.\n" );
    }
    return devName;
}

int
ClientApp::obtain_device_permissions_code_from_str(const std::string & permStr) {
    if( 9 != permStr.size() ) {
        nc_eprintf( "Can't parse device permission string %s. Expected 9-char format.\n",
                 permStr.c_str() );
        return -1;
    }
    int res = 0;
    for( unsigned char shift = 0; shift < 3; ++shift ) {
        unsigned char p = 0;
        char rc = permStr[8-(shift*3 + 2)],
             wc = permStr[8-(shift*3 + 1)],
             xc = permStr[8-(shift*3 + 0)];
        if( rc == 'r' ) { p |= 0x4; } else if( rc != '-' ) {
            nc_eprintf( "Permissions parsing error: %c. Expected 'r' or '-'.\n", rc );
            return -1;
        }
        if( wc == 'w' ) { p |= 0x2; } else if( wc != '-' ) {
            nc_eprintf( "Permissions parsing error: %c. Expected 'w' or '-'.\n", wc );
            return -1;
        }
        if( xc == 'x' ) { p |= 0x1; } else if( xc != '-' ) {
            nc_eprintf( "Permissions parsing error: %c. Expected 'x' or '-'.\n", xc );
            return -1;
        }
        res |= (p << (shift*3));
    }
    nc_dprintf( "Permissions string ``%s'' was parsed as %#04o.\n",
             permStr.c_str(), res);
    return res;
}

//
// APPLICATION METHODS
//

void
ClientApp::_V_configure( bool asDaemon ) {
    Parent::_V_configure( asDaemon );
    rapidxml::file<> xmlFile( cfgPath );
    rapidxml::xml_document<> doc;
    doc.parse<0>(xmlFile.data());
    // Obtain common parameters.
    rapidxml::xml_node<> *node = doc.first_node("ncddi")->first_node("imports");
    for( rapidxml::xml_node<> * importHostEntry = node->first_node();
             importHostEntry; importHostEntry = importHostEntry->next_sibling()) {
        std::string serverHostName( importHostEntry->first_attribute( "address" )->value() );
        int port = atoi( importHostEntry->first_attribute( "port" )->value() );
        for( rapidxml::xml_node<> * importDeviceEntry = importHostEntry->first_node();
             importDeviceEntry; importDeviceEntry = importDeviceEntry->next_sibling()) {
            # if defined(__GXX_EXPERIMENTAL_CXX0X) || __cplusplus >= 201103L
            ImportDeviceEntry newEntry {  // todo: supported only in C++11 std
                .serverAddr = serverHostName,
                .serverPort = port,
                .path = importDeviceEntry->first_attribute("path")->value(),
                .perm = importDeviceEntry->first_attribute("perm")->value(),
            };
            # else
            ImportDeviceEntry newEntry = {  // todo: supported only in C++11 std
                serverHostName,
                port,
                importDeviceEntry->first_attribute("path")->value(),
                importDeviceEntry->first_attribute("perm")->value(),
                _C11_nullptr, 0, 0
            };
            # endif
            clientCfg.imports.push_back( newEntry );
            nc_iprintf( "- Device entry to be imported: %s:%d%s[%s]\n",
                    newEntry.serverAddr.c_str(),
                    newEntry.serverPort,
                    newEntry.path.c_str(),
                    newEntry.perm.c_str());
        }
    }
}

/** For client application: read an appeal (it is always appeal) from
 * root device and invoke corresponding subroutine.
 * */ void
ClientApp::_V_treat() {
    char * readData = _C11_nullptr;
    Size writeDataLen, readDataLen = 0;
    ReentrantConnection * conn = _C11_nullptr;
    int rc = 0;
    struct Appeal appeal;
    {  // read appeal (optionally with tail data)
        bzero(&appeal, sizeof(struct Appeal));
        int rootHubDevFile = open( rootDevicePath, O_RDONLY ); {
            if( rootHubDevFile <= 0 ) {
                eraise( "Couldn't open root device by \"%s\".", rootDevicePath );
            }
            Size nRead = read( rootHubDevFile, &appeal, sizeof(struct Appeal) );
            if( nRead != sizeof(struct Appeal) ) {
                close(rootHubDevFile);
                eraise("Read %zu bytes instead of %zu.",
                       nRead, sizeof(struct Appeal));
            }
            // Find appropriate connection
            typename DevicesDictionary::const_iterator devDictEntryIt =
                _devDict.find( appeal.targetDeviceMinor );
            if( !_noNetwork ) {
                if( _devDict.end() == devDictEntryIt ) {
                    eraise( "Appeal carries wrong minor device ID: %d.\n", 
                             (int) appeal.targetDeviceMinor );
                }
                conn = devDictEntryIt->second->_conn;

                // Read tail data if needed.
                if( _nc_ef_requiresWrite & appeal.type ) {  // has tail data
                    if( e_writeOp == appeal.type ) {
                        writeDataLen = appeal.parsFor.writeOp.bufferLength;
                    } else if( _nc_eg_ioctl & appeal.type ) {
                        writeDataLen = _IOC_SIZE(appeal.parsFor.ioctlOp.cmd);
                        conn->in_buf().appeal().parsFor.ioctlOp.bfLength = writeDataLen;
                    }
                    conn->in_buf().init_for( 0,
                                    nwrq_hasAppeal | nwrq_hasBinData,
                                    writeDataLen );
                    nRead = read( rootHubDevFile,
                                  conn->in_buf().var_data(),
                                  writeDataLen );
                } else {
                    conn->in_buf().init_for( 0, nwrq_hasAppeal );
                }
                // Copy buffered appeal to network carrier.
                memcpy( &(conn->in_buf().appeal()), &appeal, sizeof(Appeal) );
                // Set host ID.
                conn->in_buf().appeal_host_id( devDictEntryIt->second->_hostID );
            }
        } close(rootHubDevFile);
    }
    Appeal * ia = _noNetwork ? &appeal : &conn->in_buf().appeal(); // shortcut
    nc_dprintf( "Entire appeal was read successfully (e_type=%d on device %d).\n",
             (int) ia->type, ia->targetDeviceMinor );
    // Dispatch appeal to device.
    if( !_noNetwork ) {
        nc_dprintf( "Sending an appeal %p of size %lub on device %d to server %s:%d.\n",
                 ia,
                 conn->in_buf().reserved(),
                 (int) ia->targetDeviceMinor,
                 conn->srv_hostname().c_str(), conn->portno() );
        try {
            if( e_openOp == ia->type ) { conn->open(); }
            conn->dispatch();
            if( e_releaseOp == ia->type ) { conn->close(); }
            # if 0   // XXX XXX XXX
            std::stringstream ss;
            conn->in_buf().str(ss);
            conn->out_buf().str(ss);
            nc_dprintf( "%s", ss.str().c_str() );
            # endif
        } catch( std::runtime_error & e ) {
            nc_eprintf( "Failed to dispatch appeal %p to %s:%d[%d]: %s.\n",
                 ia,
                 conn->srv_hostname().c_str(), conn->portno(),
                 (int) ia->targetDeviceMinor,
                 e.what() );
            if( conn->is_associated() ) conn->close();  // Prevent further usage.
            rc = -1;
        }
        if( rc >= 0 ) {
            nc_dprintf( "Request satisfied from %s:%d[%d] with %lub of data.\n",
                     conn->srv_hostname().c_str(), conn->portno(),
                     (int) ia->targetDeviceMinor,
                     conn->out_buf().reserved());
        }
        // Now, check is response really describes an appeal.
        if( conn->out_buf().status_hdr() & nwrq_rcError ) {
            nc_eprintf( "Appeal %p caused an error on server.\n", ia );
            if( conn->out_buf().content_hdr() & nwrq_hasBinData ) {
                // We have details.
                nc_eprintf( "  - details ......... : %s\n",
                        (char *) conn->out_buf().var_data() );
            }
            if( conn->out_buf().content_hdr() & nwrq_hasRCode ) {
                nc_eprintf( "  - return code ..... : %d\n",
                        conn->out_buf().rcode() );
            }
            rc = -1;
        }
        // Check, if really got a supplementary data on successive read appeal.
        if( rc >= 0 && ( _nc_ef_requiresRead & conn->out_buf().appeal().type ) ) {
            if( !conn->out_buf().content_hdr() & nwrq_hasBinData) {
                nc_eprintf( "A request did not bring any tail data while it had to.\n" );
                rc = -1;
            }
            if( _nc_ef_requiresRead & conn->out_buf().appeal().type ) {
                if( e_readOp == appeal.type ) {
                    readDataLen = conn->out_buf().appeal().parsFor.readOp.realBufLength;
                } else if( _nc_eg_ioctl & appeal.type ) {
                    readDataLen = _IOC_SIZE(conn->out_buf().appeal().parsFor.ioctlOp.cmd);
                }
                if( readDataLen != conn->out_buf().var_data_size() ||
                    readDataLen != (Size) conn->out_buf().appeal().parsFor.ioctlOp.bfLength ||
                    0 == readDataLen ) {
                    nc_eprintf( "Supposed read data length mismatch or zero: supposed %lub, got %lub.\n",
                             readDataLen, conn->out_buf().var_data_size() );
                    rc = -1;
                } else {
                    nc_dprintf( "Got %ub of additional data as a response.\n",
                         conn->out_buf().var_data_size() );
                    readData = (char*) conn->out_buf().var_data();
                }
            }
        }
    } else {
        // Testing, 'dummy' device imitation: should always return success
        // on open/release/write operations, on read yields 'XXXXX...' string.
        // Do not change offset field.
        nc_dprintf( "Device imitation fork:\n" );
        switch( ia->type ) {
            case e_llseekOp : {
                nc_dprintf( " - it is a llseek appeal.\n" );
                ia->parsFor.llseekOp.res = 0;
            } break;
            case e_readOp : {
                nc_dprintf( " - it is a read appeal.\n" );
                ia->parsFor.readOp.realBufLength = ia->parsFor.readOp.length;
                readData = (char *) malloc( ia->parsFor.readOp.length );
                if( NULL == readData ) {
                    eraise( "Failed to allocate %zu bytes for read buffer.",
                            (size_t) ia->parsFor.readOp.length );
                }
                memset( readData, 'X', ia->parsFor.readOp.length );
                readData[ ia->parsFor.readOp.length - 1 ] = '\0'; // just in case
                readDataLen = ia->parsFor.readOp.length;
            } break;
            case e_writeOp : {
                nc_dprintf( " - it is a write appeal.\n" );
                ia->parsFor.writeOp.nWrote = ia->parsFor.writeOp.bufferLength;
            } break;
            case e_openOp : {
                nc_dprintf( " - it is an open appeal.\n" );
                ia->parsFor.openOp.res = 0;
            } break;
            case e_releaseOp : {
                nc_dprintf( " - it is a release appeal.\n" );
                ia->parsFor.releaseOp.res = 0;
            } break;
            default:
                if( ia->type & _nc_eg_ioctl ) {
                    nc_dprintf( " - it is an ioctl appeal.\n" );
                    if( _IOC_DIR( ia->parsFor.ioctlOp.cmd ) & _IOC_READ ) {
                        nc_dprintf( " -- `read' flag enabled in ioctl-command.\n" );
                    }
                    if( _IOC_DIR( ia->parsFor.ioctlOp.cmd ) & _IOC_WRITE ) {
                        nc_dprintf( " -- `write' flag enabled in ioctl-command.\n" );
                    }
                    // TODO .?.
                } else {
                    nc_eprintf( "Got an unknown appeal type %d. \
Sending back to prevent module malfunction.\n", ia->type );
                }
        };
        usleep( 2000000*((double)rand())/RAND_MAX ); // imitate network delay [0,2] seconds
    } // endif nonetwork

    // Send obtained results back to kernel module.
    // TODO: use reentrant buffer instead of malloc() here.
    KMessage rMsg; bzero( &rMsg, sizeof(KMessage) );
    rMsg.data.failureDetails.devno = ia->targetDeviceMinor;
    char * chunk;
    Size chunkLen = sizeof(KMessage) ;
    if( rc >= 0 ) {
        rMsg.msgType = KMessage::ncddi_appealDispatch;
        chunkLen += sizeof(Appeal) + readDataLen;
        nc_dprintf( "Copying a respondent appeal as a chunk of size %zu = %zu + %zu + %zu.\n",
                 chunkLen, sizeof(KMessage), sizeof(Appeal),
                 readDataLen );
        chunk = (char *) malloc( chunkLen );
        memcpy( chunk,                     &rMsg,  sizeof(KMessage) );
        memcpy( chunk + sizeof(KMessage), (_noNetwork ? ia : &(conn->out_buf().appeal())), sizeof(Appeal) );
        if( readData ) {
            memcpy( chunk + sizeof(KMessage) + sizeof(Appeal),
                    readData, readDataLen );
            if( _noNetwork && readData) free(readData);
        }
    } else {
        nc_dprintf( "Sending failure message to kernel module.\n" );
        rMsg.msgType = KMessage::ncddi_appealDeliveringFailure;
        chunk = (char*) &rMsg;
    }

    nc_dprintf( "Writing response appeal.\n" );
    int rootHubDevFile = open( rootDevicePath, O_WRONLY ); {
        if( rootHubDevFile <= 0 ) {
            eraise( "Couldn't open root device by \"%s\" for writing response appeal.",
                    rootDevicePath );
        }
        write( rootHubDevFile, chunk, chunkLen );
    } close(rootHubDevFile);

    if(chunk && rc >= 0) free(chunk);
}

Thread *
ClientApp::_V_spawn_worker(DevMinorID) {
    assert(0);  // TODO: should we actually do something for client?
}

/** Submits PID to kernel module. All further messages to
 * hub device will be validated by affilation to this
 * process only.
 */
int
ClientApp::communicate_with_module() {
    int rc = 0;
    Size msgLen = sizeof(mPassphrase) + sizeof(unsigned);
    char * msgBf = (char *) malloc( msgLen );
    unsigned * pidPtr = (unsigned *) (msgBf + msgLen - sizeof(unsigned));
    *pidPtr = getpid();
    int rootHubDevFile = open( rootDevicePath, O_RDWR );
    if( rootHubDevFile <= 0 ) {
        nc_eprintf( "Couldn't open root device by path %s : %s.\n",
                 rootDevicePath, strerror(errno));
        return rc = -1;
    }
    memcpy( msgBf, mPassphrase, sizeof(mPassphrase) );
    if( write( rootHubDevFile, msgBf, msgLen ) ) {
        nc_eprintf( "Unable to send PID to kernel module: %s.\n",
                 strerror(errno) );
        rc = -1;
    }
    close(rootHubDevFile);
    free( msgBf );
    nc_dprintf( "Daemon's PID submitted to root device.\n" );
    if(!rc) {
        rc = _dispatch_import_entries();
        if( rc ) {
            nc_eprintf( "Got %d errors during importing devices.\n", -rc );
        }
    }
    return rc;
}

ClientApp::~ClientApp() {
    // tell the module that daemon is done: send
    // empty message of type ncddi_daemonQuenching.
    KMessage qMsg;
    bzero( &qMsg, sizeof(KMessage) );
    qMsg.msgType = KMessage::ncddi_daemonQuenching;
    send_meta_msg(qMsg);
}

int
ClientApp::_dispatch_import_entries() const {
    int rc = 0;
    KMessage iMsg;
    bzero( &iMsg, sizeof(KMessage) );
    iMsg.msgType = KMessage::ncddi_addEntry;
    for( std::vector<ImportDeviceEntry>::const_iterator it = clientCfg.imports.begin();
         it != clientCfg.imports.end(); ++it) {
        nc_dprintf( "Importing device ``%s''.\n", it->path.c_str() );
        std::string devName = obtain_raw_device_name( it->path );
        if( devName.empty() ) {
            nc_eprintf( "Device ``%s'' won't be imported.\n", it->path.c_str() );
            rc -= 1;
            continue;
        }
        strncpy(iMsg.data.importDevice.name, devName.c_str(), DEVICE_NAMELEN);
        iMsg.data.importDevice.permissions = obtain_device_permissions_code_from_str( it->perm );
        if( iMsg.data.importDevice.permissions < 0 ) {
            nc_eprintf( "Device ``%s'' won't be imported.\n", it->path.c_str() );
            rc -= 1;
            continue;
        }
        ReentrantConnection * conn = _C11_nullptr;
        if( !_noNetwork ) {
            conn = _check_in_import_device( *it );
            if( _C11_nullptr == conn ) {
                nc_eprintf( "Checking-in of device ``%s'' failed.\n",
                         it->path.c_str() );
                rc -= 1;
                continue;
            }
        }
        it->_conn = conn;
        if( send_meta_msg( iMsg ) ) {
            nc_eprintf( "Failed to send import message for mimic device ``%s''.\n",
                it->path.c_str() );
            rc -= 1;
        }

        int rootDeviceFD = open( "/dev/" NCDDI_DEVICE_NAME "_root", O_RDWR );
        if( rootDeviceFD <= 0 ||
            sizeof(DevMinorID) != read(rootDeviceFD, &(it->_clientID), sizeof(DevMinorID)) ||
            0 == it->_clientID ) {
            nc_eprintf( "Failed to obtain minor ID for device ``%s'':\"%s\".\n",
                it->path.c_str(), strerror(errno) );
            rc -= 1;
            if( rootDeviceFD > 0 ) close(rootDeviceFD);
        } else {
            _devDict[it->_clientID] = &(*it);
            nc_iprintf( "Device ``%s'' of minor %d imported from %s:%d[%d].\n",
                     it->path.c_str(),
                     it->_clientID,
                     ( _noNetwork ? "<imitate>" : it->_conn->srv_hostname().c_str()),
                     ( _noNetwork ? 0 : it->_conn->portno() ),
                     it->_hostID );  // todo: permissions
        }
    }
    return rc;
}

ReentrantConnection *
ClientApp::_check_in_import_device( const ImportDeviceEntry & iDevEntry ) const {
    ReentrantConnection * conn = _C11_nullptr;
    char * parcel;
    try {
        // Check, if host machine is already known.
        for( typename ConnectionsSet::iterator it = _connections.begin();
             it != _connections.end(); ++it ) {
            if( (*it)->srv_hostname() == iDevEntry.serverAddr ) {
                conn = *it;
            }
        }
        // If it is not, then create connection.
        if( _C11_nullptr == conn ) {
            conn = new ReentrantConnection( iDevEntry.serverAddr,
                                            iDevEntry.serverPort );
        }

        conn->in_buf().init_for( nwrq_rcCheckIn, nwrq_hasBinData,
                                 iDevEntry.path.size() + iDevEntry.perm.size() + 2 );
        parcel = (char *) conn->in_buf().var_data();
        memcpy( parcel,
                iDevEntry.path.c_str(),
                iDevEntry.path.size() + 1 );
        memcpy( parcel + 1 + iDevEntry.path.size() + 1,
                iDevEntry.perm.c_str(),
                iDevEntry.perm.size() + 1 );

        conn->open();
        conn->dispatch( );
        conn->close();

        char statHdr = conn->out_buf().status_hdr(),
             contHdr = conn->out_buf().content_hdr();

        if( nwrq_hasBinData & statHdr ) {
            if( nwrq_hasBinData & contHdr ) {
                nc_eprintf( "Device ``%s''(%s) check-in issues: \"%s\".\n",
                     iDevEntry.path.c_str(), iDevEntry.perm.c_str(),
                     (char *) conn->out_buf().var_data());
            } else {
                nc_eprintf( "Device ``%s''(%s) check-in failure.\n",
                     iDevEntry.path.c_str(), iDevEntry.perm.c_str() );
            }
            delete conn;
            conn = _C11_nullptr;
        } else {
            # ifndef INTREPID_NETWORKING
            if(!(nwrq_hasRCode & contHdr)) { 
                eraise( "Got malformed check-in response for server." );
            }
            # endif
            iDevEntry._hostID = conn->out_buf().rcode();
            _connections.insert( conn );
            nc_iprintf( "Device ``%s''(%s) is ready to be imported from host %s:%d[%d].\n",
                     iDevEntry.path.c_str(),
                     iDevEntry.perm.c_str(),
                     iDevEntry.serverAddr.c_str(),
                     iDevEntry.serverPort,
                     (int) iDevEntry._hostID );
        }
    } catch( std::runtime_error & e ) {
        if( conn ) delete conn;
        nc_eprintf( "Error while checking-in device:\n" );
        nc_eprintf( "  - device: ... : %s\n", iDevEntry.path.c_str() );
        nc_eprintf( "  - host ...... : %s:%d\n", iDevEntry.serverAddr.c_str(),
                                              iDevEntry.serverPort );
        nc_eprintf( "  - permissions : %s\n", iDevEntry.perm.c_str() );
        nc_eprintf( "  - reason .... : %s\n", e.what() );
        return _C11_nullptr;
    }
    return conn;
}

}  // namespace ncddi


//
// ENTRY POINT
//

static void print_usage( const char * utilname ) {
    fprintf( stderr, "Usage: %s [-c config] [-ns]\n\
 -c path to config file.\n\
 -n tells application to do not become a daemon.\n\
 -s (DEVELOPMENT) standalone mode: cuses app do not actually\n\
send any appeals to server, but to predictible react on appeals\n\
submitted to mimic devices. When is enabled, all the operations\n\
will be succeed. open()/close()/write() calls have no side \n\
effects; a read() call always returns a string \"XX...X\\0\"",
            utilname );
}

int
main(int argc, char * argv[]) {
    nc_dprintf( "NOTE: sizeof(struct Appeal)=%zu.\n", sizeof(struct Appeal) );
    # if 0
    # define PRINTOUT_TYPE( type ) \
    nc_dprintf( "\tsizeof(" XSTR(type) ") = %zu\n", sizeof(type) );

    PRINTOUT_TYPE(  int8_t  );
    PRINTOUT_TYPE(  int16_t );
    PRINTOUT_TYPE(  int32_t );
    PRINTOUT_TYPE(  int64_t );
    nc_dprintf( "\t --\n" );
    PRINTOUT_TYPE( uint8_t  );
    PRINTOUT_TYPE( uint16_t );
    PRINTOUT_TYPE( uint32_t );
    PRINTOUT_TYPE( uint64_t );
    nc_dprintf( "\t --\n" );
    PRINTOUT_TYPE( Size  );
    PRINTOUT_TYPE( BufferID );
    PRINTOUT_TYPE( UByte );
    PRINTOUT_TYPE( RCType );
    PRINTOUT_TYPE( DevMinorID );
    PRINTOUT_TYPE( DevMajorID );
    nc_dprintf( "\t --\n" );
    PRINTOUT_TYPE( Appeal::OperationParameters );
    PRINTOUT_TYPE( Appeal::OperationParameters::LlseekOp    );
    PRINTOUT_TYPE( Appeal::OperationParameters::ReadOp      );
    PRINTOUT_TYPE( Appeal::OperationParameters::WriteOp     );
    PRINTOUT_TYPE( Appeal::OperationParameters::OpenOp      );
    PRINTOUT_TYPE( Appeal::OperationParameters::ReleaseOp   );
    PRINTOUT_TYPE( Appeal::OperationParameters::IoctlOp   );

    Appeal::OperationParameters::IoctlOp s;
    bzero(&s, sizeof(s));
    # define SETMASK(what) memset( &s.what, 0xff, sizeof(s.what) );
    SETMASK( rbfID );
    SETMASK( cmd );
    SETMASK( bfLength );
    SETMASK( arg );
    SETMASK( res );
    printf( "[" );
    for( unsigned short i = 0; i < sizeof(s); ++i ) {
        printf( "\n%02d ", (int) i );
        for( unsigned short j = 0; j < 8; ++j ) {
            printf( "%d", ( *(((char *) &s)+i) & (1 << j) ? 1 : 0 ) );
        }
    }
    printf( "\n]\n" );
    return -1;
    # else
    # if 1  // check permissions string parsing
    bool noDaemon = false,
         standaloneTests = false;
    {  // configure application basics
        int opt;
        while((opt = getopt(argc, argv, "?hsnc:")) != -1) {
            switch (opt) {
            case 'n':
                noDaemon = true;
                break;
            case 's':
                standaloneTests = true;
                break;
            case '?':
            case 'h':
                print_usage(argv[0]);
                return EXIT_FAILURE;
            case 'c':
                strcpy(ncddi::App::cfgPath, optarg);
                break;
            default: /* '?' */
                print_usage(argv[0]);
                return EXIT_FAILURE;
            }
        }
    }

    ncddi::ClientApp app( standaloneTests );
    app.configure( !noDaemon );
    if( app.communicate_with_module() ) {
        nc_eprintf("Erors occuried during initial procedure. Quenching the daemon.\n");
        return EXIT_FAILURE;
    }
    try {
        app.self().run();
    } catch( std::runtime_error & e ) {
        nc_eprintf( "Abrupt daemon quenching due to error: %s.\n",
                 e.what() );
        return EXIT_FAILURE;
    }
    nc_iprintf( "Quenching daemon normally.\n" );
    return EXIT_SUCCESS;
    # else
    ncddi::ClientApp::obtain_device_permissions_code_from_str( argv[1] );
    return 0;
    # endif
    # endif
}

