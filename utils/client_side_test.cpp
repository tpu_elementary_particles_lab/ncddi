/* The MIT License (MIT)
 * 
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * */

# include <iostream>
# include <cstdlib>
# include <cstdio>
# include <cerrno>
# include <cstring>
# include <sys/stat.h>
# include <fcntl.h>
# include <asm-generic/ioctl.h>
# include <stropts.h>
# include <unistd.h>
# include "ncddi_proto.h"

//
// Testing routines
//

# ifdef RIF
# undef RIF
# endif

# define RIF( expr ) while(1) { if( expr ) {                \
    fprintf(stderr, "Met error on \"" #expr "\": %s.\n",    \
    strerror(errno) );                                      \
    return -errno;                                          \
} break;};

int
test_reading( const std::string & devPath, ssize_t nBytes ) {
    int devFile;
    char * buffer = (char *) malloc(nBytes);
    ssize_t nRead;
    RIF(  0 >= (devFile = open(devPath.c_str(), O_RDONLY )) ); {
        if( nBytes != (nRead = read( devFile, buffer, nBytes )) ) {
            fprintf(stderr, "Read %zd bytes instead of %zd: %s.\n",
                    nRead, nBytes,
                    strerror(errno) );
            return -errno;
        }
    } RIF( 0 > close(devFile) );
    printf( "Read %zu bytes:\n", nBytes );
    for( ssize_t i = 0; i < nBytes; ) {
        for( unsigned char j = 0; j < 8 && i < nBytes; ++j, ++i ) {
            printf( "\t%d", (int) *(buffer + i) );
        }
        printf( "\n" );
    }
    free(buffer);
    return EXIT_SUCCESS;
}

int
test_reading_w_offset( const std::string & devPath, ssize_t nBytes ) {
    if( nBytes < 1 ) { return EXIT_FAILURE; }
    int devFile;
    char * buffer = (char *) malloc(nBytes);
    ssize_t nRead, nToRead = nBytes/2;
    off_t offset = 0;
    RIF(  0 >= (devFile = open(devPath.c_str(), O_RDONLY )) ); {
        RIF( nToRead != (nRead = pread( devFile, buffer, nToRead, offset )) );
        offset += nToRead;
        RIF( nBytes - nToRead != (nRead = pread( devFile, buffer + nToRead, nBytes - nToRead, offset )) );
    } RIF( 0 > close(devFile) );
    printf( "Read %zu bytes:\n", nBytes );
    for( ssize_t i = 0; i < nBytes; ) {
        for( unsigned char j = 0; j < 8 && i < nBytes; ++j, ++i ) {
            printf( "\t%d", (int) *(buffer + i) );
        }
        printf( "\n" );
    }
    free(buffer);
    return EXIT_SUCCESS;
}

int
test_writing( const std::string & devPath, ssize_t nBytes ) {
    if( nBytes < 1 ) { return EXIT_FAILURE; }
    int devFile;
    char * buffer = (char *) malloc(nBytes);
    // Fill buffer with symbols to form a distinct string.
    const char tstString[] = "All work and no play make Jack a dull boy.",
               * c = tstString;
    for( ssize_t i = 0; i < nBytes; ++i, ++c ) {
        if( c == tstString + sizeof(tstString)-1 ) { c = tstString; }
        buffer[i] = *c;
    }
    buffer[nBytes-1] = '\0';

    ssize_t nWrote;
    RIF(  0 >= (devFile = open(devPath.c_str(), O_WRONLY )) ); {
        if( nBytes != (nWrote = write( devFile, buffer, nBytes )) ) {
            fprintf(stderr, "Wrote %zd bytes instead of %zd: %s.\n",
                    nWrote, nBytes,
                    strerror(errno) );
            return -errno;
        }
    } RIF( 0 > close(devFile) );
    printf( "Wrote %zu bytes:\n", nBytes );
    printf( "%s\n", buffer );
    free(buffer);
    return EXIT_SUCCESS;
}

int
test_writing_w_offset( const std::string & devPath, ssize_t nBytes ) {
    if( nBytes < 1 ) { return EXIT_FAILURE; }
    int devFile;
    char * buffer = (char *) malloc(nBytes);
    // Fill buffer with symbols to form a distinct string.
    const char tstString[] = "All work and no play make Jack a dull boy.",
               * c = tstString;
    for( ssize_t i = 0; i < nBytes; ++i, ++c ) {
        if( c == tstString + sizeof(tstString)-1 ) { c = tstString; }
        buffer[i] = *c;
    }
    buffer[nBytes-1] = '\0';

    ssize_t nWrote, nToWrite = nBytes/2;
    off_t offset = 0;
    RIF(  0 >= (devFile = open(devPath.c_str(), O_WRONLY )) ); {
        RIF( nToWrite != (nWrote          = pwrite( devFile, buffer, nToWrite, offset )) );
        offset += nToWrite;
        RIF( nBytes - nToWrite != (nWrote = pwrite( devFile, buffer + nToWrite,
                                                    nBytes - nToWrite, offset )) );
    } RIF( 0 > close(devFile) );
    printf( "Wrote %zu bytes:\n", nBytes );
    printf( "%s\n", buffer );
    free(buffer);
    return EXIT_SUCCESS;
}

int
test_open_release( const std::string & devPath ) {
    int devFile;
    RIF(  0 >= (devFile = open(devPath.c_str(), O_RDONLY )) ); {
        // Do nothing here.
    } RIF( 0 > close(devFile) );
    return EXIT_SUCCESS;
}

int
test_ioctl( const std::string & devPath, ssize_t nBytes ) {
    int devFile;
    struct IOCTLTest tstStruct;
    int cmd = 0;
    bzero( &tstStruct, sizeof(tstStruct) );
    tstStruct.clientAppealSize = sizeof(struct Appeal);
    RIF(  0 >= (devFile = open(devPath.c_str(), O_RDONLY )) ); {
        cmd = _IOC( _IOC_NONE, 0xaa, 1, 0 );
        RIF( -1 == ioctl( devFile, cmd ));
        printf( "void ioctl ok.\n" );

        cmd = _IOC( _IOC_READ, 0xaa, 2, sizeof(struct IOCTLTest) );
        RIF( -1 == ioctl( devFile, cmd, &tstStruct ));
        RIF( sizeof(struct Appeal) != tstStruct.serverAppealSize );
        printf( "read ioctl ok.\n" );
        bzero( &tstStruct, sizeof(tstStruct) );
        tstStruct.clientAppealSize = sizeof(struct Appeal);

        cmd = _IOC( _IOC_WRITE, 0xaa, 3, sizeof(struct IOCTLTest) );
        RIF( -1 == ioctl( devFile, cmd, &tstStruct ));
        //RIF( sizeof(struct Appeal) != tstStruct.serverAppealSize ); // do nothing here
        printf( "write ioctl ok.\n" );
        bzero( &tstStruct, sizeof(tstStruct) );
        tstStruct.clientAppealSize = sizeof(struct Appeal);

        cmd = _IOC( _IOC_WRITE|_IOC_READ, 0xaa, 4, sizeof(struct IOCTLTest) );
        RIF( -1 == ioctl( devFile, cmd, &tstStruct ));
        RIF( tstStruct.clientAppealSize != tstStruct.serverAppealSize );
        printf( "read-write ioctl ok.\n" );
    } RIF( 0 > close(devFile) );
    return EXIT_SUCCESS;
}

# undef RIF

//
// Entry point
//

void
print_usage( const char * appName ) {
    fprintf(stderr, "SYNTAX:\n\
\t%s <path_to_device> <open-release|read|write|positional-read|positional-write> [nbytes]\n\
Where \"device\" should be any mimic device. Provides testing I/O operations to ensure\n\
that kernel module correctly communicates with hub process.\n", appName);
}

int
main(int argc, char * argv[]) {
    
    if( argc < 2 ) {
        print_usage(argv[0]);
    }

    std::string devPath(argv[1]),
                opName(argv[2]);

    size_t nBytes = (argc == 4 ? atol(argv[3]) : 32);

    if( "read" == opName ) {
        return test_reading( devPath, nBytes );
    } else if( "positional-read" == opName ) {
        return test_reading_w_offset( devPath, nBytes );
    } else if( "write" == opName ) {
        return test_writing( devPath, nBytes );
    } else if( "open-release" == opName ) {
        return test_open_release( devPath );
    } else if( "positional-write" == opName ) {
        return test_writing_w_offset( devPath, nBytes );
    } else if( "ioctl" == opName ) {
        return test_ioctl( devPath, nBytes );
    } else {
        fprintf( stderr, "Error: invalid testing task token: %s.\n", opName.c_str() );
        print_usage( argv[0] );
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

