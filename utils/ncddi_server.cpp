/* The MIT License (MIT)
 * 
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * */

# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <stropts.h>
# include <sstream>  // XXX
# include <stropts.h>
# include <asm-generic/ioctl.h>
# include "ncddi_cfg.h"
# include "ncddi_nw.hpp"
# include "ncddi_proto.h"
# include "ncddi_app.hpp"
# include "rapidxml-1.13/rapidxml.hpp"
# include "rapidxml-1.13/rapidxml_utils.hpp"

namespace ncddi {

class CharDeviceServer :
        public App,
        public Server {
public:
    typedef App Parent;
    struct ExportDeviceEntry {
        DevMinorID ID;   /// ID of entry.
        std::string path;
        int fd;             /// is 0 when closed or was not opened.
    };
    # if defined(__GXX_EXPERIMENTAL_CXX0X) || __cplusplus >= 201103L
    typedef std::unordered_map<unsigned short, ExportDeviceEntry> ExportsDictionary;
    # else
    typedef std::map<unsigned short, ExportDeviceEntry> ExportsDictionary;
    # endif
    /// Server's extension of App's CommonConfig.
    struct ServerConfig : public CommonConfig {
        int portno;
        ExportsDictionary exports;
    };
    static int serverRC;  // Set after server quenching.
private:
    char inBuffer[NETWORK_IO_BUFFER_LENGTH];
    char outBuffer[NETWORK_IO_BUFFER_LENGTH];
protected:
    ServerConfig serverCfg;
    bool _fakeDevices;
protected:
    virtual int _treat_device_appeal(
                    ExportDeviceEntry & device,
                    const struct Appeal & ia,
                    const DataBuffer & in,
                    DataBuffer & out);

    virtual int _treat_device_appeal_fake(
                    ExportDeviceEntry & device,
                    const struct Appeal & ia,
                    const DataBuffer & in,
                    DataBuffer & out);
    
    /// Returns re-built ioctl command and prepares the out buffer.
    virtual int _treat_device_ioctl_appeal(
                    const struct Appeal & ia,
                    const DataBuffer & in,
                    DataBuffer & out,
                    int64_t & arg);

    virtual void _V_configure( bool asDaemon ) _C11_override;
    virtual void _V_treat() _C11_override;
    virtual int _V_treat_request( const DataBuffer & in,
                                        DataBuffer & out ) _C11_override;
    bool _validate_export( ExportDeviceEntry & eDevEntry );
public:
    CharDeviceServer( bool fakeDevices );
    ~CharDeviceServer();

    virtual void run() { serverRC = Server::run( serverCfg.portno ); }
};  // class CharDeviceServer


//
// STATIC ROUTINES
//

int CharDeviceServer::serverRC = -1;

//
// APPLICATION METHODS
//

CharDeviceServer::CharDeviceServer(bool fakeDevices=false) : _fakeDevices(fakeDevices) {
    _cCfgPtr = &serverCfg;
}

CharDeviceServer::~CharDeviceServer() {
    // I'm suppose we should do some destructive and evil things here...
}

bool
CharDeviceServer::_validate_export( ExportDeviceEntry & eDevEntry ) {
    // First, check if device file really exists.
    struct stat sb;
    if( -1 == stat( eDevEntry.path.c_str(), &sb ) ) {
        nc_eprintf( "stat() failed for file \"%s\" : \"%s\".\n",
            eDevEntry.path.c_str(),
            strerror(errno) );
        return false;
    }
    if( !((sb.st_mode & S_IFMT) & S_IFCHR) ) {
        nc_eprintf( "\"%s\" is not a character device.\n",
                 eDevEntry.path.c_str() );
        return false;
    }
    return true;
}

void
CharDeviceServer::_V_configure( bool asDaemon ) {
    unsigned short lastID = 1;
    Parent::_V_configure( asDaemon );
    rapidxml::file<> xmlFile( cfgPath );
    rapidxml::xml_document<> doc;
    doc.parse<0>(xmlFile.data());
    // Obtain common parameters.
    rapidxml::xml_node<> * exportsNode = doc.first_node("ncddi")->first_node("exports");
    serverCfg.portno = atoi( exportsNode->first_attribute("port")->value() );
    nc_iprintf( "Server listening port: %d.\n", serverCfg.portno );
    for( rapidxml::xml_node<> * exportDeviceEntry = exportsNode->first_node( "device" );
             exportDeviceEntry; exportDeviceEntry = exportDeviceEntry->next_sibling()) {
        # if defined(__GXX_EXPERIMENTAL_CXX0X) || __cplusplus >= 201103L
        ExportDeviceEntry newEntry {  // todo: supported only in C++11 std
            .ID = 0,
            .path = exportDeviceEntry->first_attribute("path")->value(),
            .fd = 0,
        };
        # else
        ExportDeviceEntry newEntry = {  // todo: supported only in C++11 std
            .ID = 0,
            .path = exportDeviceEntry->first_attribute("path")->value(),
            .fd = 0,
        };
        # endif
        if( !_fakeDevices ) {
            if( !_validate_export( newEntry ) ) {
                nc_eprintf( "Device ``%s'' will not be exported.\n",
                          newEntry.path.c_str() );
                continue;
            }
        } else {
            nc_iprintf( "Server application is running in pretend mode. \
Validation of ``%s'' avoided.\n", newEntry.path.c_str() );
        }
        newEntry.ID = ++lastID;
        serverCfg.exports[newEntry.ID] = newEntry;
        nc_dprintf( "Device ``%s'' added with ID=%d.\n",
                 newEntry.path.c_str(), newEntry.ID );
    }
}

void
CharDeviceServer::_V_treat() {
    // Do nothing here.
}

//
// APPEAL TREATMENT ROUTINES
//

# define FMT_RESPONSE( ... ) while(1){                              \
            snprintf( errBf, 256, __VA_ARGS__ );                    \
            out.init_for( nwrq_rcError, nwrq_hasBinData,            \
                          strlen(errBf)+1 );                        \
            strcpy( (char*) out.var_data(), errBf );                \
            nc_eprintf( " <-- \"%s\"\n", errBf );                      \
            return -2;                                              \
        break; }

int
CharDeviceServer::_treat_device_ioctl_appeal(
                    const struct Appeal & ia,
                    const DataBuffer & in,
                    DataBuffer & out,
                    int64_t & arg ) {
    UByte statFlag = nwrq_rcOk,
          contFlag = nwrq_hasAppeal;
    int hostCmd = 0;  // Actual command;
    nc_dprintf( "Got an `ioctl' appeal:\n" );
    if( (_nc_ef_requiresRead | _nc_ef_requiresWrite) & ia.type ) {
        nc_dprintf( "  - ioctl implies I/O;\n" );
        if( _nc_ef_requiresRead & ia.type ) {
            // reading required
            nc_dprintf( "  - ioctl requires read;\n" );
            out.init_for( statFlag, contFlag | nwrq_hasBinData,
                          ia.parsFor.ioctlOp.bfLength
                        );
            memcpy( &(out.appeal()), &(ia), sizeof(struct Appeal) );
            arg = (int64_t) out.var_data();
            if( _nc_ef_requiresWrite & ia.type ) {
                // reading and writing required
                nc_dprintf( "  - ioctl requires reading and writing;\n" );
                memcpy( out.var_data(),
                        in.var_data(),
                        in.var_data_size() );
                hostCmd = _IOC( 
                    _IOC_READ|_IOC_WRITE,
                    ia.parsFor.ioctlOp.deviceChar,
                    ia.parsFor.ioctlOp.functionNum,
                    ia.parsFor.ioctlOp.bfLength
                );
            } else {
                // reading only required
                hostCmd = _IOC( 
                    _IOC_READ,
                    ia.parsFor.ioctlOp.deviceChar,
                    ia.parsFor.ioctlOp.functionNum,
                    ia.parsFor.ioctlOp.bfLength
                );
            }
        } else {
            // only writing required
            nc_dprintf( "  - ioctl requires only writing;\n" );
            out.init_for( statFlag, contFlag );
            memcpy( &(out.appeal()), &(ia), sizeof(struct Appeal) );
            arg = (int64_t) in.var_data();
            hostCmd = _IOC( 
                    _IOC_WRITE,
                    ia.parsFor.ioctlOp.deviceChar,
                    ia.parsFor.ioctlOp.functionNum,
                    ia.parsFor.ioctlOp.bfLength
                );
        }
    } else {
        nc_dprintf( "  - ioctl implies no file I/O. Setting arg to %lu.\n",
                 ia.parsFor.ioctlOp.arg
               );
        out.init_for( statFlag, contFlag );
            memcpy( &(out.appeal()), &(ia), sizeof(struct Appeal) );
        arg = ia.parsFor.ioctlOp.arg;
        hostCmd = _IOC(
            _IOC_NONE,
            ia.parsFor.ioctlOp.deviceChar,
            ia.parsFor.ioctlOp.functionNum,
            0
        );
    }
    return hostCmd;
}

/**@!brief Apply a real appeal on real device.
 */ int
CharDeviceServer::_treat_device_appeal(
            ExportDeviceEntry & devEntry,
            const struct Appeal & ia,
            const DataBuffer & in,
            DataBuffer & out) {
    char errBf[256];
    int rc = 1;  // all fine, keep connection alive

    # define FINALIZE_APPEAL(expr)                                  \
            if( !(expr) ) {                                         \
                statFlag |= nwrq_rcError;                           \
                contFlag |= nwrq_hasRCode;                          \
            }                                                       \
            out.init_for( statFlag, contFlag );                     \
            if( !(expr) ) {                                         \
                out.rcode( errno );                                 \
            }                                                       \
            memcpy( &(out.appeal()), &(ia), sizeof(struct Appeal) );

    UByte statFlag = nwrq_rcOk,
          contFlag = nwrq_hasAppeal;
    // Check, if file descriptor is really opened for operations,
    // that requires it.
    if( (ia.type & (_nc_eg_FIO | _nc_eg_ioctl)) &&
         0 == devEntry.fd ) {
        FMT_RESPONSE( "File descriptor is not available while appeal requires it to be." );
    }
    switch( ia.type ) {
        case e_llseekOp : {
            nc_dprintf( "Got a `llseek' appeal.\n" );
            off_t res = lseek( devEntry.fd,
                               ia.parsFor.llseekOp.offset,
                               ia.parsFor.llseekOp.whence);
            FINALIZE_APPEAL(res > 0);
            out.appeal().parsFor.llseekOp.res = res;
        } break;
        case e_readOp : {
            nc_dprintf( "Got a `read' appeal.\n" );
            if( 0 == ia.parsFor.readOp.length ) {
                nc_eprintf( "Requested reading of 0 length data.\n" );  // just warn
            }
            out.init_for( statFlag, contFlag | nwrq_hasBinData, ia.parsFor.readOp.length );
            memcpy( &(out.appeal()), &(ia), sizeof(struct Appeal) );
            out.appeal().parsFor.readOp.realBufLength = 
                    read( devEntry.fd, out.var_data(), out.var_data_size() );
        } break;
        case e_writeOp : {
            nc_dprintf( "Got a `write' appeal.\n" );
            out.init_for( statFlag, contFlag );
            memcpy( &(out.appeal()), &(ia), sizeof(struct Appeal) );
            out.appeal().parsFor.writeOp.nWrote = 
                write( devEntry.fd, in.var_data(), in.var_data_size() );
        } break;
        case e_openOp : {
            nc_dprintf( "Got an `open' appeal.\n" );
            # ifndef INTREPID_NETWORKING
            if( 0 != devEntry.fd ) {
                FMT_RESPONSE( "Device is already opened." );
            }
            # endif
            devEntry.fd = open( devEntry.path.c_str(),
                                ia.parsFor.openOp.flags );
            FINALIZE_APPEAL(devEntry.fd > 0);
            out.appeal().parsFor.openOp.res = devEntry.fd;
        } break;
        case e_releaseOp : {
            nc_dprintf( "Got a `release' appeal.\n" );
            # ifndef INTREPID_NETWORKING
            if( 0 == devEntry.fd ) {
                FMT_RESPONSE( "Device isn't opened." );
            }
            # endif
            int res = close( devEntry.fd );
            FINALIZE_APPEAL(res == 0);
            out.appeal().parsFor.releaseOp.res = res;
        } break;
        default:
            if( _nc_eg_ioctl & ia.type ) {
                int64_t arg = 0;
                int hostCmd = _treat_device_ioctl_appeal( ia, in, out, arg );
                nc_dprintf( "Ioctl command: re-coded=%x, original=%x.\n",
                        hostCmd, ia.parsFor.ioctlOp.cmd );
                out.appeal().parsFor.ioctlOp.res =
                        ioctl( devEntry.fd, hostCmd, arg);
            } else {
                FMT_RESPONSE( "Unknown appeal type %d.", ia.type );
            }
    };
    nc_dprintf( "Appeal treatment done.\n" );
    return rc;
}

/**@!brief Imitates device reaction.
 */ int
CharDeviceServer::_treat_device_appeal_fake(
            ExportDeviceEntry & devEntry,
            const struct Appeal & ia,
            const DataBuffer & in,
            DataBuffer & out) {
    char errBf[256];
    int rc = 1;  // all fine, keep connection alive

    UByte statFlag = nwrq_rcOk,
          contFlag = nwrq_hasAppeal;

    switch( ia.type ) {
        case e_llseekOp : {
            nc_dprintf( "It is a llseek appeal.\n" );
            out.init_for( statFlag, contFlag );

            memcpy( &(out.appeal()), &(ia), sizeof(struct Appeal) );

            out.appeal().parsFor.llseekOp.res = 0;
        } break;
        case e_readOp : {
            nc_dprintf( "It is a read appeal for length=%lu.\n",
                     ia.parsFor.readOp.length );
            if( 0 == ia.parsFor.readOp.length ) {
                nc_eprintf( "Requested reading of 0 length data.\n" );  // just warn
            }
            out.init_for( statFlag, contFlag | nwrq_hasBinData, ia.parsFor.readOp.length );

            memcpy( &(out.appeal()), &(ia), sizeof(struct Appeal) );

            out.appeal().parsFor.readOp.realBufLength = ia.parsFor.readOp.length;
            memset( out.var_data(), 'X', ia.parsFor.readOp.length );
            out.var_data()[ ia.parsFor.readOp.length - 1 ] = '\0'; // just in case
            nc_dprintf( "Sending back %lu[%lu] bytes at tail as we got a read appeal.\n",
                     out.appeal().parsFor.readOp.realBufLength,
                     ia.parsFor.readOp.length);
        } break;
        case e_writeOp : {
            nc_dprintf( "It is a write appeal.\n" );
            out.init_for( statFlag, contFlag );

            memcpy( &(out.appeal()), &(ia), sizeof(struct Appeal) );

            out.appeal().parsFor.writeOp.nWrote = ia.parsFor.writeOp.bufferLength;
        } break;
        case e_openOp : {
            nc_dprintf( "It is an open appeal.\n" );
            out.init_for( statFlag, contFlag );

            memcpy( &(out.appeal()), &(ia), sizeof(struct Appeal) );

            out.appeal().parsFor.openOp.res = 0;
        } break;
        case e_releaseOp : {
            nc_dprintf( "It is a release appeal.\n" );
            out.init_for( statFlag, contFlag );

            memcpy( &(out.appeal()), &(ia), sizeof(struct Appeal) );

            out.appeal().parsFor.releaseOp.res = 0;
            rc = 0; // close connection
        } break;
        default:
            if( _nc_eg_ioctl & ia.type ) {
                int64_t arg = 0;
                int hostCmd = _treat_device_ioctl_appeal( ia, in, out, arg );
                nc_dprintf( "Ioctl command: re-coded=%x, original=%x.\n",
                hostCmd, ia.parsFor.ioctlOp.cmd );
                out.appeal().parsFor.ioctlOp.res = 0;
                if( _nc_ef_requiresWrite & ia.type ) {
                    if( sizeof(struct IOCTLTest) == in.var_data_size() ) {
                        const IOCTLTest & tstStruct = *((const IOCTLTest*) (in.var_data()));
                        if( tstStruct.clientAppealSize != sizeof(struct Appeal) ) {
                            nc_eprintf( "An ioctl() test proves an appeal mismatch: \
client:%zu vs. host:%zu!\n", tstStruct.clientAppealSize, sizeof(struct Appeal) );
                        } else {
                            nc_dprintf( "All fine with ioctl-write appeal.\n" );
                        }
                    } else {
                        nc_eprintf( "Writing requested of non-testing struct. Ignored.\n" );
                    }
                }
                if( _nc_ef_requiresRead & ia.type ) {
                    if( sizeof(struct IOCTLTest) == out.var_data_size() ) {
                        IOCTLTest & tstStruct = *((IOCTLTest*) (out.var_data()));
                        tstStruct.serverAppealSize = sizeof(struct Appeal);
                        nc_dprintf( "An ioctl-read appeal reached destination. Sending back.\n" );
                    } else {
                        nc_eprintf( "Reading requested of non-testing struct. Ignored.\n" );
                    }
                }
            } else {
                FMT_RESPONSE( "Unknown appeal type %d.", ia.type );
            }
    };
    usleep( 2000000*((double)rand())/RAND_MAX ); // imitate network delay [0,2] seconds
    nc_dprintf( "Response to appeal imitated.\n" );
    return rc;
}

/** Should return 0 on close, <0 on error, >0 to keep client connection active.
 */
int
CharDeviceServer::_V_treat_request( const DataBuffer & in,
                                          DataBuffer & out ) {
    char errBf[256];
    int rc = 1;  // all fine, keep connection alive

    // First, let's see what we've got here.
    if( in.content_hdr() & nwrq_hasAppeal ) {  // we've got an appeal
        nc_dprintf( "Got an appeal.\n" );
        const struct Appeal & ia = in.appeal();
        DevMinorID devID = in.appeal_host_id();

        typename ExportsDictionary::iterator devIt =
            serverCfg.exports.find(devID);
        # ifndef INTREPID_NETWORKING
        if( serverCfg.exports.end() == devIt ) {
            FMT_RESPONSE( "Unknown device ID=%d.", (int) devID );
        }
        # endif
        if(!_fakeDevices) {
            rc = _treat_device_appeal( devIt->second, ia, in, out );
        } else {
            rc = _treat_device_appeal_fake( devIt->second, ia, in, out );
        }
    } else if( in.status_hdr() & nwrq_rcCheckIn ) {  // it is check-in message
        char * parcel = (char *) in.var_data();
        std::string ciDevicePath( parcel ),
                    ciDevicePerm( parcel + ciDevicePath.size()+1 );
        if( ciDevicePath.empty() || ciDevicePerm.empty() ) {
            FMT_RESPONSE( "Got empty device path or permissions string." );
        }
        nc_iprintf( "Recieved check-in request for device ``%s'' (%s).\n",
                 ciDevicePath.c_str(), ciDevicePerm.c_str() );
        // Check, if device entry exists.
        ExportDeviceEntry * entry = _C11_nullptr;
        for( typename ExportsDictionary::iterator it = serverCfg.exports.begin();
             it != serverCfg.exports.end(); ++it ) {
            if( it->second.path == ciDevicePath ) {
                entry = &(it->second);
            }
        }
        if( _C11_nullptr == entry ) {
            FMT_RESPONSE( "No device ``%s'' found among %d export entires.", \
                          ciDevicePath.c_str(), (int) serverCfg.exports.size() );
        }
        out.init_for( nwrq_rcOk, nwrq_hasRCode );
        out.rcode( entry->ID );
        return 0;  // close connection after check-in
    } else {
        FMT_RESPONSE( "Got unknown network message descriptor: [0x%X;0x%X].",
                      (int) in.status_hdr(),
                      (int) in.content_hdr() );
        rc = -1;  // abrupt connection.
    }

    # if 0   // XXX XXX XXX
    std::stringstream ss;
    in.str(ss);
    out.str(ss);
    nc_dprintf( "%s", ss.str().c_str() );
    # endif

    return rc;
}

# undef FMT_RESPONSE

}  // namespace ncddi


//
// ENTRY POINT
//

static void print_usage( const char * utilname ) {
    fprintf( stderr, "Usage: %s [-c config] [-nf]\n\
 -c path to config file.\n\
 -n tells application to do not become a daemon.\n\
 -f (DEVELOPMENT) fake devices mode: cuses app to do not actually\n\
communicate with any real device, but to predictible react on appeals\n\
submitted to them. When is enabled, all the operations\n\
will be succeed. open()/close()/write() calls have no side \n\
effects; a read() call always returns a string \"XX...X\\0\"\n",
    utilname );
}

int
main( int argc, char * argv[] ) {
    nc_dprintf( "NOTE: sizeof(struct Appeal)=%zu.\n", sizeof(struct Appeal) );
    # if 0
    return -1;
    # else
    bool noDaemon = false,
         fakeDevices = false;
    {  // configure application basics
        int opt;
        while((opt = getopt(argc, argv, "?hfnc:")) != -1) {
            switch (opt) {
            case 'n':
                noDaemon = true;
                break;
            case 'f':
                fakeDevices = true;
                break;
            case '?':
            case 'h':
                print_usage(argv[0]);
                return EXIT_FAILURE;
            case 'c':
                strcpy(ncddi::App::cfgPath, optarg);
                break;
            default: /* '?' */
                print_usage(argv[0]);
                return EXIT_FAILURE;
            }
        }
    }

    ncddi::CharDeviceServer app( fakeDevices );
    app.configure( !noDaemon );
    //try {
        app.self().run();
    //} catch( std::runtime_error & e ) {
    //    nc_eprintf( "Abrupt daemon quenching due to error: %s.\n",
    //             e.what() );
    //    return EXIT_FAILURE;
    //}
    nc_iprintf( "Daemon quenched with result = %d.\n",
             ncddi::CharDeviceServer::serverRC );
    return ncddi::CharDeviceServer::serverRC;
    # endif    
}

