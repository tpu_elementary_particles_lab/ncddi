/* The MIT License (MIT)
 * 
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * */

# include <cstdlib>
# include "ncddi_app.hpp"
# include "ncddi_nw.hpp"
# include "ncddi_logging.h"

using ncddi::strfmt;

/* Just a set of testing messages that should be transmitted between
 * client and server to check network transaction correctness.
 *      (W. Shakespeare, "Henry IV", Part I, Act II)
 * */
static char dialoguePairs[][2][256] = {
    { "I prithee, Tom, beat Cut's saddle, put a few flocks in the point; poor jade, is wrung in the withers out of all cess.",
      "Peas and beans are as dank here as a dog, and that is the next way to give poor jades the bots: this house is turned upside down since Robin Ostler died."},
    { "Poor fellow, never joyed since the price of oats rose; it was the death of him.",
      "I think this be the most villanous house in all London road for fleas: I am stung like a tench." },
    { "Like a tench! by the mass, there is ne'er a king christen could be better bit than I have been since the first cock.",
      "Why, they will allow us ne'er a jordan, and then we leak in your chimney; and your chamber-lie breeds fleas like a loach." },
    /* A special pair to finilize the dialogue. */
    { "Goodbye.",
      "Farewell." },
};

//
// Client
//

int
do_client( const char * hostname, int portno ) {

    ncddi::Client cli;
    ncddi::DataBuffer in, out;

    cli.associate( hostname, portno );
    for( unsigned char i = 0; i < 10; ++i ) {
        unsigned char n = ( 3*((unsigned long)(rand()))/RAND_MAX );
        char (* pair)[256] = dialoguePairs[n];
        nc_dprintf( "#%d dialogue pair selected.\n", (int) n );

        in.reserve( strlen(pair[0])+1 );
        strcpy( in.data<char>(), pair[0] );
        cli.dispatch_request( in, out );

        fflush(stderr);
        if( out.reserved() != (Size) strlen(pair[1])+1 ) {
            nc_eprintf( "failure: length mismatch %zu != %zu.\n",
                     out.reserved(), strlen(pair[1])+1 );
            return EXIT_FAILURE;
        }
        if( 0 != strcmp( out.data<char>(), pair[1] ) ) {
            nc_eprintf( "failure: text mismatch.\n" );
            printf( "wait for: \"%s\"\n", pair[1] );
            printf( "     got: \"%s\"\n", out.data<char>() );
            return EXIT_FAILURE;
        }
        nc_dprintf( "ok.\n" );
    }

    in.reserve( strlen(dialoguePairs[3][0])+1 );
    strcpy( in.data<char>(), dialoguePairs[3][0] );
    cli.dispatch_request( in, out );

    if( out.reserved() != (Size) strlen(dialoguePairs[3][1])+1 ) {
        nc_eprintf( "failure: length mismatch %zu != %zu.\n",
                 out.reserved(), strlen(dialoguePairs[3][1])+1 );
        return EXIT_FAILURE;
    }
    if( 0 != strcmp( out.data<char>(), dialoguePairs[3][1] ) ) {
        nc_eprintf( "failure: text mismatch.\n" );
        printf( "wait for: \"%s\"\n", dialoguePairs[3][1] );
        printf( "     got: \"%s\"\n", out.data<char>() );
        return EXIT_FAILURE;
    }

    cli.deassociate();

    return EXIT_SUCCESS;
}

//
// Server
//

class TestServer : public ncddi::Server {
protected:
    virtual int _V_treat_request( const ncddi::DataBuffer & in,
                                        ncddi::DataBuffer & out ) _C11_override;
};  // class TestServer

int
TestServer::_V_treat_request( const ncddi::DataBuffer & in,
                                    ncddi::DataBuffer & out ) {
    char nMatch = -1;
    char (* pair)[256];
    for( unsigned char i = 0; i < 4; ++i ) {
        pair = dialoguePairs[i];
        if( 0 == strcmp(pair[0], in.data<char>()) ) {
            nc_dprintf( "It is #%d dialogue pair. Sending response back.\n", (int) i );
            nMatch = i;
            break;
        }
    }
    if( -1 == nMatch ) {
        eraise( "Server got malformed data." );
    }
    out.reserve(strlen(pair[1])+1);
    strcpy( out.data<char>(), pair[1] );
    if( 3 == nMatch ) {
        nc_dprintf( "Got 'goodbye' message, quenching connection.\n" );
        return 0; // It is the last dialogue pair.
    }
    return 1; // Success, but keep client connection alive.
}


int
do_server( int portno ) {
    TestServer srv;
    return srv.run( portno );
}

//
// Entry Point
//

void
print_usage( const char * nm ) {
    fprintf(stderr, "Usage:\n\
\t$ %s <c|s> <port> [hostname]\n\
where hostname should be given only for client application (when\n\
'c' is the first argument). 's' is for server application.\n\
", nm);
}

int
main(int argc, char * argv[]) {
    srand (time(NULL));

    char role;
    int portno;
    char * hostname;
    if( argc < 3 ) {
        print_usage( argv[0] );
        return EXIT_FAILURE;
    }

    if( argv[1][0] == 'c' ) {
        role = 'c';
    } else if( argv[1][0] == 's' ) {
        role = 's';
    }
    portno = atoi( argv[2] );

    if( 'c' == role ) {
        if( argc != 4 ) {
            print_usage(argv[0]);
            return EXIT_FAILURE;
        } else {
            hostname = argv[3];
        }
    }

    nc_dprintf( "Emerging %s ", (role == 'c' ? "client" : "server" ) );
    if( 's' == role ) {
        fprintf( stderr, "on port %d.\n", portno );
        return do_server( portno );
    } else {
        fprintf( stderr, "to %s:%d.\n", hostname, portno );
        return do_client( hostname, portno );
    }
}

