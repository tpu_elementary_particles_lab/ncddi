/* NCDDI kernel module -- a part of character device network sharing tool.
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * */

/**@file ncddi.c
 *
 * Implements kernel module functionality for managing
 * network character devices.
 */

# include <linux/module.h>
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("A network character device handler.");
MODULE_AUTHOR("Renat (Crank) Dusaev");
# include <asm/siginfo.h>	    /* siginfo */
# include <asm/uaccess.h>
# include <linux/cdev.h>
# include <linux/device.h>
# include <linux/fs.h>
# include <linux/fs.h>
# include <linux/init.h>
# include <linux/kernel.h>
# include <linux/miscdevice.h>
# include <linux/mutex.h>
# include <linux/proc_fs.h>
# include <linux/rcupdate.h>	/* rcu_read_lock */
# include <linux/sched.h>
# include <linux/signal.h>
# include <linux/slab.h>
# include <linux/syslog.h>
# include <linux/types.h>
# include <linux/version.h>
# include <linux/wait.h>

# include "ncddi_mod.h"
# include "ncddi_proto.h"

static int rc;
static int lastMinorID = -1;

# define instantiate_constant( name, code, descr ) const uint8_t _nc_ ## name = code;
    for_each_appeal_descriptive_constants(instantiate_constant)
# undef instantiate_constant

/* FWDs */
static void destroy_mimic_device( int minor );
static int satisfy_appeal(DevMinorID);
static int finalize_appeal(void);

static DECLARE_WAIT_QUEUE_HEAD(appealQueue);

/** A forward declaration of file operations callback
 * dictionary struct initialized below.
 */ static const struct file_operations _fops;

/** A forward declaration of device class */
static struct class _ncddiDevClassInstance;

/** Declaration of scope containing all module's globals.
 * Represents a module's set of parameters itself.
 * */ static struct {
    struct class * ncddiClass;  /* statically assigned below */
    /** An array of devices actually allocated.
     *  Note, that root is always present when module
     *  is inserted correctly. The root has minor index
     *  and ordering number `0'.
     */ struct MimicDevice * mimicDevices;

    /** A buffer for messages coming to root from
     *  client daemon. 
     */ UByte * msgBuffer;
    /** A Last created device minor number.
     */ DevMinorID _lastMinorDeviceNum;

    /** Comes from userspace */
    pid_t clientDaemonPid;
    struct task_struct * hubT;

    /** Appeal */
    struct Appeal a;
    struct mutex aLock;
    char waitFlag;  /* used by conditional wait */
    struct RBuffer * response;  /* buffer: stores written appeal that came from server */
} self;

/*
 * Routines
 */

/** Clean-up function: destroys all (normally created)
 * mimic devices. Uses devicePtr in MimicDevice struct
 * as an indicator that device had been created.
 * */ static void
destroy_all_imported_mimics(void) {
    unsigned short i;
    for( i = 1; i < MAX_DEVICES_AVAILIBLE; ++i ) {
        if( self.mimicDevices[i].devicePtr ) {
            nc_dprintf( "Will try to destroy device %d.", i );
            destroy_mimic_device( i );
        }
    }
}

/** Acquire pointer to root device */
static struct MimicDevice * root(void) { return self.mimicDevices; }

/** Actual module constructor (initializes self-struct):
 *  - initializes whole structure with zeroes;
 *  - allocates devices
 *  - register class
 * */ static int
construct_self(void) {
    krn_bzero( &self, sizeof(self) );
    self.ncddiClass = &_ncddiDevClassInstance;
    self.clientDaemonPid = 0;
    self.hubT = NULL;
    self.waitFlag = 0;
    self._lastMinorDeviceNum = 0;
    mutex_init( &(self.aLock) );
    /* Mimic devices inner memory pool */
    self.mimicDevices = (struct MimicDevice *) kzalloc(
                    MAX_DEVICES_AVAILIBLE*sizeof(struct MimicDevice),
                    GFP_KERNEL);
    if(self.mimicDevices == NULL) {
        nc_eprintf( "Failed to allocate array of %d devices.", MAX_DEVICES_AVAILIBLE );
        return -ENOMEM;
    }
    nc_dprintf( "Mimic devices array allocated [%d]: %p, %zu bytes.",
        (int) MAX_DEVICES_AVAILIBLE,
        self.mimicDevices,
        (size_t) MAX_DEVICES_AVAILIBLE*sizeof(struct MimicDevice));
    /* Chardev array */
    rc = alloc_chrdev_region( &self.mimicDevices->cDev.dev,
                              0, MAX_DEVICES_AVAILIBLE,
                              NCDDI_DEVICE_NAME);
    if( rc < 0 ) {
        nc_eprintf( "Error registering device region (%d).", rc );
        return rc;
    }
    nc_dprintf( "chrdev region allocated." );
    /* Register class */
    rc = class_register( self.ncddiClass );
    if( rc ) {
        unregister_chrdev_region( root()->cDev.dev, MAX_DEVICES_AVAILIBLE );
        nc_eprintf( "Unable to register device class (%d).", rc ); return -EINVAL;
    }
    nc_dprintf( "Device class registered: %p", self.ncddiClass );
    /* Allocate messages buffer */
    self.msgBuffer = (UByte *) kzalloc( ROOT_BUFFER_LENGTH, GFP_KERNEL );
    if( NULL == self.msgBuffer ) {
        unregister_chrdev_region( root()->cDev.dev, MAX_DEVICES_AVAILIBLE );
        nc_eprintf( "Unable to allocate root message buffer of length %d.",
                 (int) ROOT_BUFFER_LENGTH ); return -ENOMEM;
    }
    nc_dprintf( "KMessage buffer allocated at %p of size %d bytes.",
            self.msgBuffer,
            ROOT_BUFFER_LENGTH
           );
    rbuf_init_pool();  /* For details, see ncddi_buffer.c */
    nc_dprintf( "Primary stage passed." );
    return KRN_RC_OK;
}

/** When device region and device class are registered, one
 * can create an actual device. This routine also creates
 * an actual device node in /dev (usually) via udev's
 * mechanisms.
 * */ static int
construct_mimic_device(
        int minor,
        const char * name,
        int permissions ) {
    dev_t devno = MKDEV( MAJOR(root()->cDev.dev), minor );
    struct device * device = NULL;
    self.mimicDevices[minor].permissions    = permissions;

    cdev_init( &self.mimicDevices[minor].cDev, &_fops );
    self.mimicDevices[minor].cDev.owner = THIS_MODULE;

    rc = cdev_add(&self.mimicDevices[minor].cDev, devno, 1);
    if(rc) {
        nc_eprintf( "Error %d while adding device with minor %d.",
               rc, minor); return rc;
    }
    if( minor != 0 ) {
        device = device_create(self.ncddiClass,
                               root()->devicePtr,       /* is parent device */ 
                               devno, NULL,             /* no additional data */
                               "%s", name );
    } else { /* Create roor */
        device = device_create(self.ncddiClass, NULL,   /* no parent device */ 
                           devno, NULL,                 /* no additional data */
                           name );
    }
    if(IS_ERR(device)) {
        rc = PTR_ERR(device);
        nc_eprintf("Error %d while creating device with minor %d.",
                rc, minor);
        cdev_del(&self.mimicDevices[minor].cDev);
        return rc;
    }
    self.mimicDevices[minor].devicePtr      = device;
    mutex_init( &(self.mimicDevices[minor].devLock) );
    nc_dprintf( "Device %s:%d with permissions %#o created.",
             name, minor, permissions);
    self._lastMinorDeviceNum = minor;
    return KRN_RC_OK;
}

/** Destroys character device.
 * */static void
destroy_mimic_device( int minor ) {
    cdev_del(&self.mimicDevices[minor].cDev);
    device_destroy(self.ncddiClass, MKDEV(MAJOR(root()->cDev.dev), minor));
    /*krn_bzero(&(self.mimicDevices[minor]), sizeof(struct MimicDevice));*/
    self.mimicDevices[minor].devicePtr = 0;
    lastMinorID = 0;
    nc_dprintf( "Device %d destroyed.", minor );
    return;
}

/** Treats messages sent to root (usually by client daemon)
 * */static int
treat_message(void) {
    const unsigned char * c;
    struct KMessage * msg;
    if( 0 != self.clientDaemonPid ) { /* Should be a 'KMessage' struct */
        msg = (KMessage*) self.msgBuffer;
        switch( msg->msgType ) {
            case ncddi_addEntry : {
                nc_dprintf( "Constructing new mimic device." );
                /* remained, if something goes wrong */
                self._lastMinorDeviceNum = 0;
                return construct_mimic_device(
                    ++lastMinorID,
                    msg->data.importDevice.name,
                    msg->data.importDevice.permissions );
            } break;
            case ncddi_daemonQuenching : {
                nc_dprintf( "Daemon %d detached.",
                         self.clientDaemonPid );
                self.clientDaemonPid = 0;
                self.hubT = NULL;
                destroy_all_imported_mimics();
                return 0;
            } break;
            case ncddi_appealDispatch : {
                nc_dprintf( "Module got appeal from client to dispatch." );
                if( msg->data.failureDetails.devno < 0 ||
                    msg->data.failureDetails.devno > MAX_DEVICES_AVAILIBLE ) {
                    nc_eprintf( "Preventing malfunction caused by invalid message." );
                    return -EINVAL;
                }
                self.waitFlag = 0;
                self.mimicDevices[msg->data.failureDetails.devno].lastAppealDeliveryStatus = 0;
                rc = finalize_appeal();
                wake_up_interruptible( &appealQueue );
                rbuf_free( self.response );
                return rc;
            } break;
            case ncddi_appealDeliveringFailure : {
                nc_eprintf( "Appeal delivery failed for dev %d. Unblocking expectants.",
                         (int) msg->data.failureDetails.devno );
                if( msg->data.failureDetails.devno < 0 ||
                    msg->data.failureDetails.devno > MAX_DEVICES_AVAILIBLE ) {
                    nc_eprintf( "Preventing malfunction caused by invalid message." );
                    return -EINVAL;
                }
                self.waitFlag = 0;
                self.mimicDevices[msg->data.failureDetails.devno].lastAppealDeliveryStatus = -EPIPE;
                /*rc = finalize_appeal();*/
                wake_up_interruptible( &appealQueue );
                return KRN_RC_OK;
            } break;
            default:
                return -EINVAL;
        };
    } else {  /* There should be a passphrase to set client PID */
        unsigned char n = 0;
        const unsigned char pw[] = ROOT_PID_MSG_PASSPHRASE;
        unsigned char * ts = self.msgBuffer;
        for( c = pw; *c != 0x00; ++c, ++ts, ++n ) {
            if( *c != *ts ) {
                nc_eprintf( "Invalid passphrase [%d]: 0x%02x != 0x%02x.",
                    (int) n, (unsigned int) *c, (unsigned int) *ts );
                return -EINVAL;
            }
        }
        self.clientDaemonPid = *((unsigned *) ++ts);

        rcu_read_lock();
    	self.hubT = pid_task(find_vpid(self.clientDaemonPid), PIDTYPE_PID);
	    if(NULL == self.hubT){
		    nc_eprintf("Couldn't locate task for process with PID=%d.",
                     self.clientDaemonPid );
    		rcu_read_unlock();
	    	return -EINVAL;
    	}
	    rcu_read_unlock();
        nc_dprintf( "Task located." );

        nc_iprintf( "Client daemon %d attached.", self.clientDaemonPid );
        return KRN_RC_OK;
    }
}

/** Communicates with client daemon: kicks the client daemon and blocks
 *  execution of current thread.
 *
 * This routine requires hub device to be created and client daemon PID
 * to be set at ``self'' structure. Implements following behaviour:
 *      1) Kicks the client daemon (hub process) with system signal to
 *         awake it up from a pending and hangs currently running
 *         thread.
 *      2) Hub process reads an appeal data from root device and does
 *         the treatment. During this operation current thread is pending
 *         state by wait_event_interruptible(...) routine. When hub done
 *         with this appeal it writes output data to root device and then
 *         current thread awakes.
 *      3) Being awakening it returns KRN_RC_OK.
 * */static int
satisfy_appeal(DevMinorID minorID) {
    struct siginfo info;
    /* 1. Kick the hub process. */
    if( 0 == self.clientDaemonPid || NULL == self.hubT ) {
        nc_eprintf( "Have no client daemon (hub) process attached." );
        return -EINVAL;
    }
    memset(&info, 0, sizeof(struct siginfo));
	info.si_signo = KICK_SIGNAL;
	info.si_code = /*SI_QUEUE*/ SI_KERNEL;
    rc = send_sig_info(KICK_SIGNAL, &info, self.hubT);
    /* 2. Stop execution and wait for hub to write results. */
    self.waitFlag = 1;
    wait_event_interruptible( appealQueue, 0 == self.waitFlag );
    /* 3. Return thread. */
    return self.mimicDevices[minorID].lastAppealDeliveryStatus;
}

/** Does appeal appending when it is satisfied: writes necessary fields.
 *
 * Uses self.a appeal object as an instance to be finalized with a.response
 * as submitted data. Both of them should contain Appeal instance at the
 * beginning of corresponding memory block. The a.responce can contain
 * additional data at the rest space of buffer.
 * */ static int
finalize_appeal(void) {
    struct RBuffer * bf;
    struct Appeal * r = (struct Appeal *) rbuf_data(self.response);
    rc = KRN_RC_OK;
    # define CMATCH(nm) (r-> nm != self.a. nm )
    { /* Do pre-checks */
        if( CMATCH(type) || CMATCH(targetDeviceMinor) ) {
            nc_eprintf( "Got appeal mismatch at response data (header)!" );
            rc = -EINVAL;
            goto out;
        }
    }
    switch( r->type ) {
        case e_llseekOp : {
            if( CMATCH( parsFor.llseekOp.offset ) ||
                CMATCH( parsFor.llseekOp.whence ) ) {
                nc_eprintf( "Got appeal mismatch at response data (llseek parameters)!" );
                rc = -EINVAL;
                goto out;
            }
            self.a.parsFor.llseekOp.res = r->parsFor.llseekOp.res;
        } break;
        case e_readOp : {
            if( CMATCH( parsFor.readOp.length ) ) {
                nc_eprintf( "Got appeal mismatch at response data (read parameters)!" );
                rc = -EINVAL;
                goto out;
            }
            self.a.parsFor.readOp.realBufLength = rbuf_size(self.response) -
                                                  sizeof(struct Appeal);
            /* Should be freed by read() call */
            bf = rbuf_obtain( self.a.parsFor.readOp.realBufLength );
            if( NULL == bf ) {
                nc_eprintf( "Failed to obtain/reserve read data buffer of length %d.",
                         self.a.parsFor.readOp.realBufLength );
                rc = -ENOMEM;
                goto out;
            }
            self.a.parsFor.readOp.rbfID = bf->id;
            memcpy( rbuf_data(bf),
                    rbuf_data(self.response) + sizeof(struct Appeal),
                    r->parsFor.readOp.realBufLength );
            self.a.parsFor.readOp.offset = r->parsFor.readOp.offset;
        } break;
        case e_writeOp : {
            if( CMATCH( parsFor.writeOp.bufferLength ) ) {
                nc_eprintf( "Got appeal mismatch at response data (write parameters)!" );
                rc = -EINVAL;
                goto out;
            }
            self.a.parsFor.writeOp.offset = r->parsFor.writeOp.offset;
            self.a.parsFor.writeOp.nWrote = r->parsFor.writeOp.nWrote;
        } break;
        case e_openOp : {
            self.a.parsFor.openOp.res = r->parsFor.openOp.res;
        } break;
        case e_releaseOp : {
            self.a.parsFor.releaseOp.res = r->parsFor.releaseOp.res;
        } break;
        default: {
            if( r->type & e_ioctlOp ) {
                if( CMATCH( parsFor.ioctlOp.cmd ) ||
                    CMATCH( parsFor.ioctlOp.arg ) ) {
                    nc_eprintf( "Got appeal mismatch at response data (ioctl cmd/arg)." );
                    rc = -EINVAL;
                    goto out;
                }
                if( _IOC_DIR( r->parsFor.ioctlOp.cmd ) & _IOC_READ ) {
                    /* Should be freed by ioctl() call further */
                    bf = rbuf_obtain( _IOC_SIZE(r->parsFor.ioctlOp.cmd) );
                    if( NULL == bf ) {
                        nc_eprintf( "Failed to obtain/reserve ioctl read data buffer." );
                        rc = -ENOMEM;
                        goto out;
                    }
                    self.a.parsFor.ioctlOp.rbfID = bf->id;
                    memcpy( rbuf_data(bf),
                            rbuf_data(self.response) + sizeof(struct Appeal),
                            _IOC_SIZE(r->parsFor.ioctlOp.cmd) );
                }
            } else {
                nc_eprintf( "Unknown or unsupported request appeal type: %d",
                     (int) r->type );
                rc = -EINVAL;
                goto out;
            }
        } break;
    };
out:
    nc_dprintf( "Appeal information appended with response data." );
    /* todo: check that lock will be released anyway */
    return rc;
}

/* ****************************************************************************
 * Device callbacks
 */

/** A generic macro obtaining device supp data. */
# define GENERIC_FILE_APPEAL( enumCode, optype )                    \
    struct MimicDevice * dev;                                       \
    unsigned char minor = MINOR(filp->f_dentry->d_inode->i_rdev);   \
    nc_dprintf( "Got " optype " at %d.", minor );                      \
    dev = (struct MimicDevice *) filp->private_data;                \
    if( mutex_lock_killable(&dev->devLock) ) { return -EINTR; }

# define ROUTINE_RETURN( retvalname ) \
    mutex_unlock(&dev->devLock);      \
    return retvalname;

/** Called when ... */
static long long ncddi_llseek( struct file *filp,
                               long long offset,
                               int whence) {
    long long retval;
    GENERIC_FILE_APPEAL( e_llseekOp, "llseek" );
    /* Root device doesn't support llseek */
    if( 0 == minor ) { retval = -EOPNOTSUPP; goto out; }
    /* Form new appeal and notify hub process */ {
        if( mutex_lock_killable( &self.aLock ) ) {
            retval = -EINTR;
            goto out;
        }
        self.a.type = e_llseekOp;
        self.a.targetDeviceMinor = minor;

        self.a.parsFor.llseekOp.offset = offset;
        self.a.parsFor.llseekOp.whence = whence;
        rc = satisfy_appeal( minor );
        mutex_unlock( &self.aLock );
        if( 0 != rc ) {
            retval = rc;
            goto out;
        }
        retval = self.a.parsFor.llseekOp.res;
    }
out:
    ROUTINE_RETURN(retval);
}

/** Called when 'read' system call is done on device file.
 * Note: when applied to root device when no appeal is awaited,
 * return last created mimic device's minor number if buffer size
 * is strictly equals to sizeof(DevMinorID) (yes, it's an 
 * architectural lack).
 * */
static ssize_t ncddi_read( struct file *filp,
                    char *buffer,
                    size_t length,
                    loff_t *offset) {
    struct RBuffer * bfPtr;
    ssize_t retval = 0;
    ssize_t count = 0,
            nRead = 0;
    GENERIC_FILE_APPEAL( e_readOp, "read" );
    if( length < 1 ) {
        nc_eprintf( "Ignoring reading of length <1.\n" );
        retval = -EINVAL;
        goto out;
    }

    /*
     * For root device
     * *****************/
    if( 0 == minor ) {
        /* Hub process wants to read appeal? */
        if( current->pid == self.clientDaemonPid &&
            NULL != self.hubT ) {
            /* any awaiting appeal? */
            if( 1 == self.waitFlag ) {
                nc_dprintf( "Hub process reads an appeal on root device." );
                /* Write appeal struct first. */
                if( (count = sizeof(struct Appeal) - *offset) > 0 ) {
                    if( 0 != copy_to_user( buffer, &self.a + *offset, count ) ) {
                        nc_eprintf( "Couldn't to satisfy appeal reading: copy to userspace failed." );
                        return -EFAULT;
                    }
                    nRead += count;
                } else {
                    count = -count;
                }
                length -= count;
                if( self.a.type & _nc_ef_requiresWrite ) {
                    if( e_writeOp == self.a.type ) {
                        bfPtr = rbuf_get_by_id(self.a.parsFor.writeOp.rbfID);
                    } else if( _nc_eg_ioctl & self.a.type ) {
                        bfPtr = rbuf_get_by_id(self.a.parsFor.ioctlOp.rbfID);
                    } else {
                        nc_eprintf( "Writing buffer unbound for this operation." );
                        retval = -EINVAL;
                        goto out;
                    }
                    if( NULL == bfPtr ) {
                        nc_eprintf( "Failed to obtain buffer with ID=%d.",
                                 (int) self.a.parsFor.writeOp.rbfID );
                        retval = -EINVAL;
                        goto out;
                    }
                    if( 0 != copy_to_user( buffer + count,
                                           rbuf_data(bfPtr),
                                           length ) ) {
                        nc_eprintf( "Failed to copy data to userspace." );
                        rbuf_free( bfPtr );
                        retval = -EFAULT;
                        goto out;
                    }
                    nc_dprintf( "Tail data of size %zu copyied to userspace.",
                             length );
                } else if( 0 != length ) {
                    nc_eprintf( "Reading buffer length surpass available data on root \
device for this type of appeal." );
                    length = 0;
                }
                nRead += length;
                *offset += nRead;
                retval = nRead;
            } else {
                if( length != sizeof(DevMinorID) ) {
                    nc_eprintf( "Has no ready appeal or %d != %d.",
                             current->pid, self.clientDaemonPid );
                    retval = -EINTR;
                } else {
                    if( 0 != copy_to_user(  buffer,
                                          &(self._lastMinorDeviceNum),
                                            sizeof(DevMinorID) ) ) {
                        nc_eprintf( "Failed to copy data to userspace: last mimic device minor ID." );
                        retval = -EFAULT;
                        goto out;
                    }
                    retval = sizeof(DevMinorID);
                    *offset += retval;
                }
            }
        } else {
            nc_eprintf( "Root device denies read() from PID=%d.", (int) current->pid );
            retval = -EACCES;
        }
        goto out;
    }
    /*
     * For mimic devices
     * *****************/
    /* Form new appeal and notify hub process */ {
        if( mutex_lock_killable( &self.aLock ) ) {
            retval = -EINTR;
            goto out;
        }
        self.a.type = e_readOp;
        self.a.targetDeviceMinor = minor;

        self.a.parsFor.readOp.length = length;
        self.a.parsFor.readOp.offset = *offset;
        rc = satisfy_appeal( minor );
        if( 0 != rc ) {
            nc_eprintf( "Error during satisfying appeal on mimic device." );
            retval = rc;
            goto aOut;
        }
        nc_dprintf( "Read on mimic satisfied." );
        bfPtr = rbuf_get_by_id( self.a.parsFor.readOp.rbfID );
        if( copy_to_user( buffer,
                          rbuf_data(bfPtr),
                          self.a.parsFor.readOp.realBufLength ) ) {
            nc_eprintf( "Error on copying read buffer data to user of size %u.",
                     self.a.parsFor.readOp.realBufLength );
            retval = -EFAULT;
            goto aOut;
        }
        # if 0 /*XXX*/
        {
            Size n;
            char tBf[512];
            for( n = 0; n < self.a.parsFor.readOp.realBufLength; ++n ) {
                sprintf( tBf+3*n, "%02x,", (int) rbuf_data(bfPtr)[n] );
            }
            nc_dprintf( "XXX frm krn: %s\n", tBf );
        }
        # endif

        rbuf_free(bfPtr);
        *offset += self.a.parsFor.readOp.realBufLength;
        retval = self.a.parsFor.readOp.realBufLength;
        /*nc_dprintf( "XXX read on mimic device: retval=%zd", retval );*/
aOut:
        mutex_unlock( &self.aLock );
    }
out:
    ROUTINE_RETURN( retval );
}

/** Called when 'write' system call is done on device file. */
static ssize_t ncddi_write( struct file *filp,
                     const char *buffer,
                     size_t length,
                     loff_t *offset) {
    struct RBuffer * bfPtr;
    ssize_t retval;
    struct KMessage * msg;
    size_t tailLength;
    GENERIC_FILE_APPEAL( e_writeOp, "write" );
    if( length < 1 ) {
        nc_eprintf( "Ignoring writing of length <1.\n" );
        retval = -EINVAL;
        goto out;
    }

    if( 0 != minor ) {
        /*
         * For mimic device
         * ****************/
        /* Form new appeal and notify hub process */ {
            if( mutex_lock_killable( &self.aLock ) ) {
                retval = -EINTR;
                goto out;
            }
            self.a.type = e_writeOp;
            self.a.targetDeviceMinor = minor;

            self.a.parsFor.writeOp.bufferLength = length;
            self.a.parsFor.writeOp.offset = *offset;
            if( length > MAX_ALLOWED_IO_BUF_LENGTH ) {
                nc_eprintf( "Write operation of %zu > %zu block denied.",
                          length, (size_t) MAX_ALLOWED_IO_BUF_LENGTH );
                retval = -EINVAL;
                goto aOut;
            }
            /* Should be freed after appeal was sent */
            bfPtr = rbuf_obtain( length );
            if( NULL == bfPtr ) {
                nc_eprintf( "Failed to acquire r-buffer of length %zu.", length );
                retval = -ENOMEM;
                goto aOut;
            }
            self.a.parsFor.writeOp.rbfID = bfPtr->id;
            if( 0 != copy_from_user( rbuf_data(bfPtr),
                                     buffer, length ) ) {
                rbuf_free( bfPtr );
                nc_eprintf( "Failed to copy write data from userspace of length %zu.", length );
                retval = -EFAULT;
                goto aOut;
            }
            nc_dprintf( "To-write data of length %zu replicated. Dispatching an appeal to hub.",
                     length);
            rc = satisfy_appeal( minor );
            if( 0 != rc ) {
                nc_eprintf( "Failed to satisfy write appeal." );
                rbuf_free( bfPtr );
                retval = rc;
                goto aOut;
            }
            *offset += self.a.parsFor.writeOp.nWrote;
            rbuf_free( bfPtr );
            retval = self.a.parsFor.writeOp.nWrote;
            nc_dprintf( "Mimic writing operation completed: off=%lld, rc=%zu.",
                     *offset, retval);
aOut:
            mutex_unlock( &self.aLock );
        }
        goto out;
    } else {  /* treat as root */
        /*
         * For root device
         * ***************/
        if( 0 != self.clientDaemonPid ) {
            if( length < sizeof(struct KMessage) ) {
                nc_eprintf( "Suspiciously short message to root (%d < %d).",
                         (int) length, (int) sizeof(struct KMessage));
                retval = -EINVAL;
                goto out;
            }
            if( copy_from_user( self.msgBuffer, buffer, sizeof(KMessage) ) ) {
                nc_eprintf( "Failed to read a message to root device of length %zu.",
                         length );
                retval = -EFAULT;
                goto out;
            }
            msg = (struct KMessage *) self.msgBuffer;
            if(ncddi_appealDispatch == msg->msgType ) {
                nc_dprintf( "Appeal dispatch..." );
                /* Copy the rest of buffer as it is appeal */
                tailLength = length - sizeof(struct KMessage);
                if( tailLength <= 0 ) {
                    nc_eprintf( "Internal error: empty tail for appeal carrier message!" );
                    retval = -EINVAL;
                    goto out;
                }
                /* Should be freed by treat_message() */
                self.response = rbuf_obtain( tailLength );
                if( NULL == self.response ) {
                    nc_eprintf( "Failed to allocate buffer for user data." );
                    retval = -ENOMEM;
                    goto out;
                }
                if( 0 != copy_from_user( rbuf_data( self.response ),
                                        buffer + sizeof(struct KMessage),
                                        tailLength ) ) {
                    rbuf_free( self.response );
                    nc_eprintf( "Failed to read supplementary user data." );
                    retval = -EFAULT;
                    goto out;
                }
                nc_dprintf( "Appeal dispatch done." );
            } else if( length != sizeof(struct KMessage) ){
                nc_eprintf( "Suspiciously long message to root." );
                retval = -EINVAL;
                goto out;
            }
        } else {
            /* This is the first message that should contain client PID */
            retval = copy_from_user( self.msgBuffer, buffer, length );
            if( retval < 0 ) goto out;
        }
        retval = treat_message();
    }
out:
    ROUTINE_RETURN( retval );
}

/** Called when 'open' system call is done on device file. */
static int ncddi_open( struct inode *inode,
                       struct file *filp) {
    unsigned int mj = imajor( inode ),
                 mn = iminor( inode );
    struct MimicDevice * mdev = NULL;
    nc_dprintf( "Got open at %d:%d", mj, mn );
    if( mj != MAJOR(root()->cDev.dev) || mn < 0 || mn >= MAX_DEVICES_AVAILIBLE ) {
        nc_eprintf( "Device %d:%d is not affilated to NCDDI module.",
                 mj, mn );
        return -ENODEV;
    }
    mdev = &(self.mimicDevices[mn]);
    filp->private_data = mdev;
    if( inode->i_cdev != &(mdev->cDev) ) {
        nc_eprintf( "Malformed internal data of mimic device instance %d:%d.",
                 mn, mj);
        return -EFAULT;
    }
    /* TODO: allocate buffers, or whatever ... */
    /* Do nothing for root device */
    if( 0 == mn ) {
        nc_dprintf( "Root device opened by %d.", current->pid );
        return KRN_RC_OK;
    }
    /* Form new appeal and notify hub process */ {
        if( mutex_lock_killable( &self.aLock ) ) {
            nc_eprintf( "Error acquiring appeal mutex for %d:%d.", mj, mn );
            return -EINTR;
        }
        self.a.type = e_openOp;
        self.a.targetDeviceMinor = mn;
        self.a.parsFor.openOp.flags = filp->f_flags;
        rc = satisfy_appeal( mn );
        mutex_unlock( &self.aLock );
        nc_dprintf( "Open appeal satisfied." );
        return rc ? rc : self.a.parsFor.openOp.res;
    }
}

/** Called when 'close' system call is done on device file. */
static int ncddi_release( struct inode *inode,
                          struct file *filp) {
    unsigned int mj = imajor( inode ),
                 mn = iminor( inode );
    nc_dprintf( "Got release at %d:%d", mj, mn );
    /* Do nothing for root device */
    if( 0 == mn ) { 
        return KRN_RC_OK;
    }
    /* Form new appeal and notify hub process */ {
        if( mutex_lock_killable( &self.aLock ) ) {
            return -EINTR;
        }
        self.a.type = e_releaseOp;
        self.a.targetDeviceMinor = mn;
        rc = satisfy_appeal( mn );
        mutex_unlock( &self.aLock );
        return rc ? rc : self.a.parsFor.releaseOp.res;
    }
}

/** Called when ...
 * TODO: I don't know when this particular part of API
 * had been actually changed, so may be version 3.0.0 is not
 * suitable choice (if you change this, take into account
 * file_operations-structure initialization below).
 * */
# if LINUX_VERSION_CODE <= KERNEL_VERSION(3,0,0)
static int ncddi_ioctl( struct inode *,         /* device file inode */
                        struct file * filp,     /* ??? */
                        unsigned int cmd,       /* ioctl number (of command?) */
                        unsigned long arg       /* ioctl param (possibly ptr) */
                       ) {
    int rc;
# else
static long ncddi_ioctl( struct file *filp,
                        unsigned int cmd,       /* ioctl number */
                        unsigned long arg       /* ioctl param (possibly ptr) */
                       ) {
    long rc;
# endif
    struct RBuffer * bfPtr = NULL;
    size_t argSize = 0;
    uint8_t appealTypeAcc = _nc_eg_ioctl;
    GENERIC_FILE_APPEAL( e_ioctlOp, "ioctl" );

    if( 0 == minor ) {
        /* ioctl on root device */
        nc_eprintf( "NCDDI root device ignores ioctl()." );
        return -ENOTTY; /* TODO: ? */
    } else {
        /* ioctl on mimic device:
         * 
         * Now, we suppose that provided command is alwas under generic 
         * IOCTL convention described at
         * <kernel src dir>/Documentation/ioctl/ioctl-number.txt as follows:
         *  - according to _IO-mscros, we are awaiting here:
         *      _IO    an ioctl with no parameters (do not interpret arg)
         *      _IOW   an ioctl with write parameters (use copy_from_user)
         *      _IOR   an ioctl with read parameters  (use copy_to_user)
         *      _IOWR  an ioctl with both write and read parameters (parse
         *             arg as pointer, but do both copies then).
         *  - the whole cmd will be placed into ioctl appeal and thus be
         *    submitted to real device on host. Depending on actual _IO
         *    flags, the 'arg' can be interpreted as an actual data pointer
         *    and then the data it points to will be replaced by actual
         *    pointers to memory blocks of length obtain according to
         *    <kernel src dir>/Documentation/ioctl/ioctl-decoding.txt.
         *  We use here access_ok() function to verify that provided ptrs
         *  is available.
        **/
        nc_dprintf( "Got an ioctl request with command 0x%x and argument %lu.",
                 cmd, arg );
        if( _IOC_DIR(cmd) & (_IOC_READ | _IOC_WRITE) ) {
            argSize = _IOC_SIZE(cmd);
            nc_dprintf( "Obtaining buffer of size %zu.", argSize );
            bfPtr = rbuf_obtain( argSize );
            if( NULL == bfPtr ) {
                nc_eprintf( "Failed to acquire r-buffer of length %zu.", argSize );
                rc = -ENOMEM;
                goto out;
            }
            appealTypeAcc |= _nc_eg_ioctl;
        }
        if( _IOC_DIR(cmd) & _IOC_READ ) {
            /* This ioctl cmd requires writing data to
             * userspace (from kernel point of view) */
            rc = !access_ok(VERIFY_WRITE, (void *)arg, argSize);
            if(rc) {
                nc_eprintf( "Kernel can not write to address %p+%zu, \
or ioctl() call is unconventional.",
                         (void *)arg, argSize );
                rc = -EINVAL;
                rbuf_free( bfPtr );
                bfPtr = NULL;
                goto out;
            }
            appealTypeAcc |= _nc_ef_requiresRead;
            nc_dprintf( "ioctl() call requires writing to userspace by ptr %p+%zu",
                     (void *)arg, argSize );
        }
        if( _IOC_DIR(cmd) & _IOC_WRITE ) {
            /* This ioctl cmd requires reading data from
             * userspace (from kernel point of view) */
            rc = !access_ok(VERIFY_READ, (void *)arg, argSize);
            if(rc) {
                nc_eprintf( "Kernel can not read from address %p+%zu, \
or ioctl() call is unconventional.",
                         (void *)arg, argSize );
                rc = -EINVAL;
                rbuf_free( bfPtr );
                bfPtr = NULL;
                goto out;
            }
            appealTypeAcc |= _nc_ef_requiresWrite;
            nc_dprintf( "ioctl() call requires reading from userspace by ptr 0x%p+%zu",
                     (void *)arg, argSize );
        }

        /* Form new appeal and notify hub process */ {
            if( mutex_lock_killable( &self.aLock ) ) {
                rc = -EINTR;
                if( bfPtr ) {
                    rbuf_free( bfPtr );
                    bfPtr = NULL;
                }
                goto out;
            }
            self.a.type = appealTypeAcc;
            self.a.targetDeviceMinor = minor;

            self.a.parsFor.ioctlOp.cmd = cmd;
            self.a.parsFor.ioctlOp.rbfID = bfPtr ? bfPtr->id : 0;

            self.a.parsFor.ioctlOp.deviceChar = _IOC_TYPE(cmd);
            self.a.parsFor.ioctlOp.functionNum = _IOC_NR(cmd);

            self.a.parsFor.ioctlOp.arg = arg;
            self.a.parsFor.ioctlOp.bfLength = argSize;
            if( _IOC_DIR(cmd) & _IOC_WRITE ) {
                /* This invokation requires copying something
                 * from userspace: interpret arg as pointer and copy
                 * data to buffer*/
                if( 0 != copy_from_user( rbuf_data(bfPtr),
                                         (char *)arg, argSize ) ) {
                    rbuf_free( bfPtr ); bfPtr = NULL;
                    nc_eprintf( "Failed to copy ioctl data from userspace %p of length %zu.",
                              (char *)arg, argSize );
                    rc = -EFAULT;
                    goto aOut;
                }
                nc_dprintf( "To-write data of length %zu replicated. Dispatching an appeal to hub.",
                         argSize);
            }
            /* Send the damn appeal */
            rc = satisfy_appeal( minor );
            if( 0 != rc ) {
                nc_eprintf( "Error during satisfying ioctl appeal on mimic device." );
                goto aOut;
            }
            nc_dprintf( "ioctl on mimic satisfied." );
            if( _IOC_DIR(cmd) & _IOC_READ ) {
                /* This invokation requires copying something
                 * to userspace: interpret arg as pointer and copy
                 * data from reentrant buffer*/
                bfPtr = rbuf_get_by_id( self.a.parsFor.ioctlOp.rbfID );
                if( NULL == bfPtr ) {
                    nc_eprintf( "Failed to re-acquire r-buffer of ID=%d.",
                             (int) self.a.parsFor.ioctlOp.rbfID );
                    rc = -ENOMEM;
                    goto out;
                }
                if( copy_to_user( (char*)arg,
                                  rbuf_data(bfPtr),
                                  argSize ) ) {
                    nc_eprintf( "Error on copying read buffer data to user of size %u.",
                             self.a.parsFor.readOp.realBufLength );
                    rc = -EFAULT;
                    goto aOut;
                }
            }
            nc_dprintf( "Mimic ioctl operation completed." );  /* TODO: details */
aOut:
            if( bfPtr ) {
                rbuf_free( bfPtr ); bfPtr = NULL;
            }
            mutex_unlock( &self.aLock );
        }
    }
out:
    ROUTINE_RETURN(rc);
}

# undef GENERIC_FILE_APPEAL
# undef ROUTINE_RETURN

/* ****************************************************************************
 * Entry points functions and structs
 */

static int
__init ncddi_init(void) {
    /* Allocate primary pools (without appeals) and register class */
    construct_self();
    /* Construct root first */
    lastMinorID = 0;
    # if 1
    construct_mimic_device( lastMinorID,
                            NCDDI_DEVICE_NAME "_root",
                            0666 /*--rw-rw-rw-*/ );
    # endif
    nc_dprintf( "Module initialized." );
    return KRN_RC_OK;
} 

static void
__exit ncddi_exit(void) {
    nc_dprintf( "Quenching module." );
    if( self.mimicDevices ) { destroy_mimic_device(0); destroy_all_imported_mimics(); }
    nc_dprintf( "Devices destroyed." );
    if( self.ncddiClass ) { class_unregister(self.ncddiClass); }
    nc_dprintf( "Class unregistered." );  /*Note: do not destroy it!*/
    unregister_chrdev_region(self.mimicDevices->cDev.dev, MAX_DEVICES_AVAILIBLE);
    nc_dprintf( "Device region unregistered." );
    if( self.msgBuffer ) { kfree(self.msgBuffer); }
    nc_dprintf( "KMessage buffer freed." );
    rbuf_clear_pool();
    nc_dprintf( "Module quenched." );
}

static const struct file_operations _fops = {
    .owner      = THIS_MODULE,      .llseek     = ncddi_llseek,
    .read       = ncddi_read,       .write      = ncddi_write,
    # if LINUX_VERSION_CODE <= KERNEL_VERSION(3,0,0)
    .ioctl        = ncddi_ioctl,
    # else
    .unlocked_ioctl = ncddi_ioctl,
    # endif
    .open       = ncddi_open, .release    = ncddi_release
};


static int rc;  /** Holds resulting code in many routines below. */
/** A device class method that appends udev's environment with
 * correct permission rules.
 * */ static int mimic_device_uevent(
        struct device *dev,
        struct kobj_uevent_env *env) {
    int minor = MINOR(dev->devt);
    struct MimicDevice * mmDevPtr;
    if( minor < 0 || minor > MAX_DEVICES_AVAILIBLE ) {
        nc_eprintf( "Wrong minor number (%d) at mimic_device_uevent!", minor );
    }
    mmDevPtr = self.mimicDevices + minor;
    nc_dprintf( "Setting up permissions of mimic device %d to %#o",
              minor, mmDevPtr->permissions );
    add_uevent_var(env, "DEVMODE=%#o", mmDevPtr->permissions);
    return 0;
}

/** A device class to be registered */
static struct class _ncddiDevClassInstance = {
    .name       = NCDDI_DEVICE_NAME"_devCls",
    .owner      = THIS_MODULE,
    .dev_uevent = mimic_device_uevent,
};

module_init(ncddi_init);
module_exit(ncddi_exit);

