/* NCDDI kernel module -- a part of character device network sharing tool.
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * */

# ifndef H_NCDDI_KERNEL_MODULE_H
# define H_NCDDI_KERNEL_MODULE_H

# include "ncddi_cfg.h"
# ifndef __KERNEL__
#  include <stdint.h>
#  include <stdlib.h>
#  include <assert.h>
#  include "ncddi_logging.h"
#  define heap_alloc( n ) ( (int8_t*) malloc(n) )
#  define heap_free( what ) free( what )
#  define memzero( w, n ) bzero( w, n )
# else
#  include <linux/types.h>
#  include <linux/kernel.h>
#  include <linux/cdev.h>
#  include <linux/device.h>
#  include <linux/slab.h>
#  define heap_alloc( n ) ( (int8_t*) kmalloc(n, GFP_KERNEL) )
#  define heap_free( what ) kfree( what )
# if 0
typedef __s8    int8_t;
typedef __s16   int16_t;
typedef __s32   int32_t;
typedef __u8    uint8_t;
typedef __u16   uint16_t;
typedef __u32   uint32_t;
# endif
# endif

struct RBuffer {
    int8_t state;
    
    Size hugeLen;
    int8_t * huge;
    int8_t preallocd[KERNEL_IO_BUFFER_LENGTH];

    Size nReserved;

    BufferID id;
};

/** Kernel return code (ok). */
# define KRN_RC_OK 0

# ifdef __KERNEL__
/** A single device entry for internal module DB.
 */
struct MimicDevice {
    struct cdev cDev;
    struct device * devicePtr;
    int permissions;
    struct mutex devLock;
    int lastAppealDeliveryStatus;
    /*...*/
};
# endif

/** Reserves reentrant buffer to hold data of given length. */
int         rbuf_reserve( struct RBuffer * bfPtr, Size toReserve );
/** Returns data pointer provided by buffer. */
int8_t *    rbuf_data( struct RBuffer * bfPtr );
/** Currently reserved data length. */
Size        rbuf_size( struct RBuffer * bfPtr );

/** Initialized pool of reentrant buffers.*/
void rbuf_init_pool(void);
/** Returns vacant reentrant buffer from pool; do not forget to rbuf_free() it. */
struct RBuffer * rbuf_obtain( Size toReserve );
/** Returns not-vacant buffer by its ID. */
struct RBuffer * rbuf_get_by_id( BufferID id );
/** Marks buffer from pool as 'vacant' so that it further can be re-used. */
void rbuf_free( struct RBuffer * bfPtr );
/** Actually frees all data allocated during lifecycle. */
void rbuf_clear_pool(void);

# endif  /* H_NCDDI_KERNEL_MODULE_H */

