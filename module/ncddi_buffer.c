/* NCDDI kernel module -- a part of character device network sharing tool.
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * */

# include "ncddi_mod.h"

# ifndef __KERNEL__
# define LSTREND "\n"
# else
# define LSTREND
# endif

static const int8_t kbf_Huge    = 0x1;
static const int8_t kbf_Allocd  = 0x2;
static const int8_t kbf_Vacant  = 0x4;

static int rbuf_free_huge( struct RBuffer * bfPtr ) {
    heap_free( bfPtr->huge );
    bfPtr->huge     = NULL;
    bfPtr->hugeLen  = 0;
    bfPtr->state   &= ~kbf_Huge; /* disable 'huge' flag */
    return 0;
}

static int rbuf_alloc_huge( struct RBuffer * bfPtr, Size n ) {
    bfPtr->huge     = heap_alloc(n);
    if( NULL == bfPtr->huge ) {
        return -1;
    }
    bfPtr->hugeLen  = n;
    bfPtr->state   |= kbf_Huge;  /* enable 'huge' flag */
    return 0;
}

static void
rbuf_init( struct RBuffer * bfPtr ) {
    bfPtr->state        = 0;
    bfPtr->hugeLen      = 0;
    bfPtr->huge         = NULL;
    bfPtr->nReserved    = 0;
    bfPtr->id           = 0;
}

int
rbuf_reserve( struct RBuffer * bfPtr, Size toReserve ) {
    int rc = 0;
    if( toReserve > KERNEL_IO_BUFFER_LENGTH ) {
        if((bfPtr->state & kbf_Allocd) &&
           (bfPtr->state & kbf_Huge) ) {
            if( toReserve > bfPtr->hugeLen ) {
                if( 0 > (rc = rbuf_free_huge(bfPtr)) ) goto out;
                if( 0 > (rc = rbuf_alloc_huge(bfPtr, toReserve) ) ) goto out;
            }
        } else {
            if( 0 > (rc = rbuf_alloc_huge(bfPtr, toReserve) ) ) goto out;
        }
    }
    if( 0 == rc ) {
        bfPtr->nReserved = toReserve;
        bfPtr->state |= kbf_Allocd;
    }
out:
    return rc;
}

int8_t *
rbuf_data( struct RBuffer * bfPtr ) {
    # ifndef NDEBUG
    if( NULL == bfPtr ) {
        nc_eprintf( "!DE: NULL rbuffer ptr." LSTREND );
        return NULL;
    }
    if(!(bfPtr->state & kbf_Allocd)) {
        nc_eprintf( "!DE: unallocated buffer data acquizition." LSTREND );
        return NULL;
    }
    # endif
    if(!(bfPtr->state & kbf_Huge)) return bfPtr->preallocd;
    return bfPtr->huge;
}

Size
rbuf_size( struct RBuffer * bfPtr ) {
    # ifndef NDEBUG
    if( NULL == bfPtr ) {
        nc_eprintf( "!DE: NULL rbuffer ptr." LSTREND );
        return 0;
    }
    # endif
    if(!(bfPtr->state & kbf_Allocd)) return 0;
    return bfPtr->nReserved;
}

static void
rbuf_clear( struct RBuffer * bfPtr ) {
    # ifndef NDEBUG
    if( NULL == bfPtr ) {
        nc_eprintf( "!DE: NULL rbuffer ptr." LSTREND );
    }
    # endif
    if(!(bfPtr->state & kbf_Allocd)) return;
    if(!(bfPtr->state & kbf_Huge  )) return;
    rbuf_free_huge( bfPtr );
}

/*
 * Pool stuff
 */

static struct RBuffer __rBufferPool[KERNEL_IO_BUFFER_POOL_NUMBER];

void
rbuf_init_pool(void) {
    Size n;
    for( n = 0; n < KERNEL_IO_BUFFER_POOL_NUMBER; ++n ) {
        struct RBuffer * it = __rBufferPool + n;
        rbuf_init(it);
        it->state |= kbf_Vacant;
        it->id = n;
    }
    nc_dprintf( "Pool of %d reentrant r-buffers initialized." LSTREND,
              (int) KERNEL_IO_BUFFER_POOL_NUMBER );
}

struct RBuffer *
rbuf_obtain( Size toReserve ) {
    BufferID n;
    for( n = 0; n < KERNEL_IO_BUFFER_POOL_NUMBER; ++n ) {
        struct RBuffer * it = __rBufferPool + n;
        if(it->state & kbf_Vacant) {
            it->state &= ~kbf_Vacant;
            rbuf_reserve( it, toReserve );
            return it;
        }
    }
    nc_eprintf( "Vacant buffer not found." LSTREND );
    return NULL;
}

struct RBuffer *
rbuf_get_by_id( BufferID id ) {
    struct RBuffer * bfPtr;
    if( id < 0 || id > KERNEL_IO_BUFFER_POOL_NUMBER ) {
        nc_eprintf( "Wrong r-buffer ID: !(0 <= %d < %d" LSTREND,
                 (int) id,
                 (int) KERNEL_IO_BUFFER_POOL_NUMBER );
        return NULL;
    }
    bfPtr = __rBufferPool + id;
    # ifndef NDEBUG
    if(bfPtr->state & kbf_Vacant) {
        nc_eprintf( "!DE: r-buffer %d is vacant (code malfunction)." LSTREND, id );
        return NULL;
    }
    # endif
    return bfPtr;
}

void
rbuf_free( struct RBuffer * bfPtr ) {
    # ifndef NDEBUG
    if( NULL == bfPtr ) {
        nc_eprintf( "!DE: NULL rbuffer ptr to free." LSTREND );
        return;
    }
    if(!(bfPtr->state & kbf_Allocd)) {
        nc_eprintf( "!DE: unallocated r-buffer %d to free." LSTREND, bfPtr->id );
        return;
    }
    # endif
    bfPtr->state |= kbf_Vacant;
}

void
rbuf_clear_pool(void) {
    BufferID n;
    for( n = 0; n < KERNEL_IO_BUFFER_POOL_NUMBER; ++n ) {
        struct RBuffer * it = __rBufferPool + n;
        if(!(it->state & kbf_Vacant)) {
            nc_eprintf( "r-buffer %d is still in use." LSTREND, (int) it->id );
            continue;
        }
        rbuf_clear(__rBufferPool + n);
    }
    nc_dprintf( "Pool of %d reentrant r-buffers cleared." LSTREND,
              (int) KERNEL_IO_BUFFER_POOL_NUMBER );
}

