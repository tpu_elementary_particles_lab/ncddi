/* The MIT License (MIT)
 * 
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * */

# include <stdio.h>
# include <string.h>
# include <errno.h>
# include <pthread.h>
# include <stdarg.h>
# include <unistd.h>
# include <time.h>
# include "ncddi_logging.h"

/** Logging file struct ptr. */
static FILE * logfile = NULL;
static pthread_mutex_t logfileLock;

int
open_log_file( const char * filename ) {
    int rc;
    if( !logfile ) {
        pthread_mutex_init(&logfileLock, NULL);
    }
    pthread_mutex_lock(&logfileLock);
    if( logfile && stderr != logfile ) {
        time_t ltime;
        ltime = time(NULL);
        rc = nc_log_print( ncddi_info,
                        "=== Process %d detached (due to log switching) from log at %s",
                        getpid(), asctime( localtime(&ltime) ) );
        fclose(logfile);
    }
    logfile = fopen( filename, "a" );
    pthread_mutex_unlock(&logfileLock);
    if( !logfile ) {
        logfile = stderr;
        nc_log_print( ncddi_critical,
                   "Couldn't open log file \"%s\": %s.\n",
                   filename, strerror(errno) );
        rc = -1;
    } else {
        time_t ltime;
        ltime = time(NULL);
        rc = nc_log_print( ncddi_info,
                        "=== Process %d attached to log at %s",
                        getpid(), asctime( localtime(&ltime) ) );
    }
    return rc;
}

int
close_log_file() {
    int rc;
    if( logfile && stderr != logfile ) {
        time_t ltime;
        ltime = time(NULL);
        rc = nc_log_print( ncddi_info,
                        "=== Process %d detached from log at %s",
                        getpid(), asctime( localtime(&ltime) ) );
        fclose(logfile);
    } else {
        rc = -1;
    }
    return rc;
}

int
nc_log_print( UByte msgType, const char * fmt, ... ) {
    char prefix;
    switch( msgType ) {
        case ncddi_debug : {
            prefix = 'd';
        } break;
        case ncddi_info : {
            prefix = 'i';
        } break;
        case ncddi_warning : {
            prefix = 'w';
        } break;
        case ncddi_error : {
            prefix = 'e';
        } break;
        case ncddi_critical : {
            prefix = 'E';
        } break;
    };
    pthread_mutex_lock(&logfileLock);
    if( logfile ) {
        fprintf(logfile, "[%c] ", prefix);
        va_list args;
        va_start(args, fmt);
            vfprintf(logfile, fmt, args);
        va_end(args);
        fflush( logfile );
    } else {
        fprintf(stderr, "[%c] ", prefix);
        va_list args;
        va_start(args, fmt);
            vfprintf(stderr, fmt, args);
        va_end(args);
        fflush( stderr );
    }
    pthread_mutex_unlock(&logfileLock);
    return 0;
}

