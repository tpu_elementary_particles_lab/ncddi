/* The MIT License (MIT)
 * 
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * */

# include <arpa/inet.h>
# include "ncddi_nw.hpp"
# include "ncddi_app.hpp"
# include "ncddi_logging.h"

namespace ncddi {

//
// Client
//

Client::Client() {}

Client::~Client() {
    if( is_associated() ) {
        deassociate();
    }
}

void
Client::associate(  const std::string & hostnameStr,
                    int portno ) {
    _sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if( _sockfd < 0 ) {
        eraise("Couldn't open socket: \"%s\"", strerror(errno) );
    }
    _server = gethostbyname( hostnameStr.c_str() );
    if( _server == NULL ) {
        eraise("Host lookup failed: %s\n", hostnameStr.c_str() );
    }
    bzero((char *) &_srvAddr, sizeof(struct sockaddr_in));
    _srvAddr.sin_family = AF_INET;
    bcopy((char *)_server->h_addr, 
          (char *)&_srvAddr.sin_addr.s_addr,
          _server->h_length);
    _srvAddr.sin_port = htons(portno);
    if( connect(_sockfd,(struct sockaddr *) &_srvAddr, sizeof(_srvAddr)) < 0 ) {
        eraise("Socket connect() failed: \"%s\"", strerror(errno));
    }
    _associated = true;
    nc_dprintf( "(Cli): instance %p associated with %s:%d\n",
              this, hostnameStr.c_str(), portno );
}

void
Client::deassociate() {
    if( _sockfd > 0 ) {
        close( _sockfd );
    }
}

void
Client::dispatch_request( const DataBuffer & in,
                          DataBuffer & out ) {
    in.write( _sockfd );
    out.read( _sockfd );
    nc_dprintf( "(Cli): request transacton cycle %zu/%zu succeed.\n",
             in.reserved(), out.reserved());
    # ifdef DEVELOPMENT_IOBUFFERS_DATA_VERBOSE
    char tBf[512];
    for( Size n = 0; n < in.reserved(); ++n ) {
        sprintf( tBf+3*n, "%02x,", (int) *(in.data<unsigned char>(n)) );
    }
    nc_dprintf( "(Cli) in: %s\n", tBf );
    for( Size n = 0; n < out.reserved(); ++n ) {
        sprintf( tBf+3*n, "%02x,", (int) *(out.data<unsigned char>(n)) );
    }
    nc_dprintf( "(Cli)out: %s\n", tBf );
    # endif
}

//
// Server
//

Server::Server() {}

Server::~Server() {
    if( _sockfd > 0 ) {
        close(_sockfd);
    }
}

int
Server::run( int portno ) {
    int bindRc; int yes = 1;
    (void) yes;  // supress 'unused variable'
                 // warning when no setsockopt() enabled.
    int newsockfd;      // socket descr acquired when incoming transm-n occured
    socklen_t clilen;

    RIF( 0 >= (_sockfd = socket(AF_INET, SOCK_STREAM, 0)) );
    bzero((char *) &_srvAddr, sizeof(struct sockaddr_in));

    _srvAddr.sin_family = AF_INET;
    _srvAddr.sin_addr.s_addr = INADDR_ANY;
    _srvAddr.sin_port = htons(portno);

    # ifdef REUSABLE_SOCKET
    // Silly workaround in case of incorrect server quenching.
    RIF( 0 > setsockopt( _sockfd,
                         SOL_SOCKET,
                         SO_REUSEPORT | SO_REUSEADDR,
                         (char *) &yes, sizeof(int) ) );
    # endif  // REUSABLE_SOCKET

    # if 0
    // This can be enabled to disable Nagel's buffering and speed-up transmissions
    // of incoming data. From unix socket FAQ:
    // """TCP_NODELAY is for a specific purpose; to disable the Nagle buffering
    //  algorithm. It should only be set for applications that send frequent
    //  small bursts of information without getting an immediate response,
    //  where timely delivery of data is required (the canonical example
    //  is mouse movements)."""
    // However, for NCDDI we always have an immediate feedback.
    RIF( 0 > setsockopt(  _sockfd,
                          IPPROTO_TCP,     // set option at TCP level
                          TCP_NODELAY,
                          (char *) &yes,
                          sizeof(int));
    # endif

    RIF( 0 > (bindRc = bind(_sockfd,
                   (struct sockaddr *) &_srvAddr,
                   sizeof(struct sockaddr_in))));

    RIF( 0 != listen(_sockfd, MAX_DEVICES_AVAILIBLE) );

    DataBuffer in, out;
    while(1) {
        clilen = sizeof(_cliAddr);
        // They says "non-negative" at $ man 3p accept.
        RIF( 0 >= (newsockfd = accept(_sockfd,
                                      (struct sockaddr *) &_cliAddr,
                                      &clilen)));
        nc_dprintf( "Incoming connection from %s client initiated.\n",
                 inet_ntoa(_cliAddr.sin_addr) );
        int rc = 1;
        do {
            nc_dprintf( "(Srv) 1) Reading incoming data from %s.\n", inet_ntoa(_cliAddr.sin_addr) );
            in.read( newsockfd );
            nc_dprintf( "(Srv) 2) Treating request from %s.\n",      inet_ntoa(_cliAddr.sin_addr) );
            rc = _V_treat_request( in, out );
            nc_dprintf( "(Srv) 3) Sending response of size %lu back to %s.\n",
                        out.reserved(),
                        inet_ntoa(_cliAddr.sin_addr) );
            out.write( newsockfd );
            # ifdef DEVELOPMENT_IOBUFFERS_DATA_VERBOSE
            char tBf[512];
            for( Size n = 0; n < in.reserved(); ++n ) {
                sprintf( tBf+3*n, "%02x,", (int) *(in.data<unsigned char>(n)) );
            }
            nc_dprintf( "(Srv) in: %s\n", tBf );
            for( Size n = 0; n < out.reserved(); ++n ) {
                sprintf( tBf+3*n, "%02x,", (int) *(out.data<unsigned char>(n)) );
            }
            nc_dprintf( "(Srv)out: %s\n", tBf );
            # endif
        } while( rc > 0 );
        in.clear();
        out.clear();
        close(newsockfd);
        nc_dprintf( "(Srv) Incoming connection with %s client closed %s.\n",
              inet_ntoa(_cliAddr.sin_addr),
              (rc == 0 ? "normally" : "due to an error"));
    }
    close(_sockfd);
    _sockfd = 0;

    return EXIT_SUCCESS;
}

}  // namespace ncddi

