/* The MIT License (MIT)
 * 
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * */

# include <cstring>
# include <cerrno>
# include <memory>
# include <cstdarg>
# include "ncddi_app.hpp"
# include "rapidxml-1.13/rapidxml.hpp"
# include "rapidxml-1.13/rapidxml_utils.hpp"

namespace ncddi {

//
// Generic Thread
//

void *
Thread::thread_func(void *d){ ((Thread *)d)->run(); return NULL; }

Thread::Thread(){}

Thread::~Thread(){}

void
Thread::run() {
    _V_run();
}

int
Thread::start() {
    return pthread_create(&_thread, NULL, Thread::thread_func, (void*)this);
}

int
Thread::wait() {
    return pthread_join  (_thread, NULL);
}

void
Thread::queue_push(Appeal * inst) {
    pthread_mutex_lock( &tasksLock );
    tasks.push(inst);
    pthread_mutex_unlock( &tasksLock );
}

Appeal *
Thread::queue_pull() {
    Appeal * res;
    pthread_mutex_lock( &tasksLock );
    res = tasks.front();
    tasks.pop();
    pthread_mutex_unlock( &tasksLock );
    return res;
}

bool
Thread::is_queue_empty() const {
    bool rs;
    pthread_mutex_lock( &tasksLock );
    rs = tasks.empty();
    pthread_mutex_unlock( &tasksLock );
    return rs;
}

//
// Generic app
//

char App::cfgPath[256] = "ncddi.xml";

App * App::_self = NULL;

App & App::self() {
    assert(_self);
    return *_self;
}

void
App::configure( bool asDaemon ) {
    this->_V_configure( asDaemon );  // by default doesn nothing
}

void
App::spawn_worker( DevMinorID devMinorNumber ) {
    # if 0
    // Create new worker and insert to dictionary.
    _devWDict.insert( std::pair<DevMinorID, Thread*>(
        devMinorNumber, _V_spawn_worker(devMinorNumber)) );
    # else
    eraise( "Threading unsupported." );  // TODO
    # endif
}

void
App::queue_push( DevMinorID id, Appeal * appeal ) {
    # if 0
    std::unordered_map<DevMinorID, Thread *>::iterator it =
        _devWDict.find( id );
    if( _devWDict.end() == it ) {
        // TODO: spawn (?) thread if appeal is openOp/ioctlOp type,
        // or report an error if it is not.
    } else {
        it->second->queue_push( appeal );
    }
    # else
    eraise( "Threading unsupported." );  // TODO
    # endif
}

void
App::quench() {
    assert(0); // TODO: wait all threads to be done or for a timeout and return
}


void
App::_V_configure( bool asDaemon ) {
    nc_iprintf("Using config file: %s\n", cfgPath );
    rapidxml::file<> xmlFile( cfgPath );
    rapidxml::xml_document<> doc;
    doc.parse<0>(xmlFile.data());
    // Obtain common parameters.
    rapidxml::xml_node<> *node = doc.first_node("ncddi");
    _cCfgPtr->logFilePath   = node->first_attribute( "logFile" )->value();
    _cCfgPtr->pidFilePath   = node->first_attribute( "pidFile" )->value();
    _cCfgPtr->bufferLength  = atoi(node->first_attribute( "bufferLength" )->value());
    _cCfgPtr->nDevMax       = atoi(node->first_attribute( "maxDevicesN" )->value());
    // Check, whether another daemon process running.
    if( access(_cCfgPtr->pidFilePath.c_str(), F_OK) != -1 ) {
        nc_eprintf( "Found PID-file %s. One should stop another daemon or delete it manually.\n",
                 _cCfgPtr->pidFilePath.c_str() );
        exit( EXIT_FAILURE );
    }
    // Attach logfiles.
    if( open_log_file(_cCfgPtr->logFilePath.c_str()) ) {
        exit(1);
    }
    // Daemonize if needed.
    if(asDaemon) {
        if( daemon(0, 1) ) {
            nc_eprintf( "Unable to daemonize process: %s\n",
                    strerror(errno) );
            exit(EXIT_FAILURE);
        }
    }
    _cCfgPtr->thisPID = getpid();
    // Make a PID-file.
    FILE * pidFileDescriptor = fopen( _cCfgPtr->pidFilePath.c_str(), "w" );
    if( !pidFileDescriptor ) {
        nc_log_print( ncddi_critical,
                   "Couldn't open PID file %s : %s.\n",
                   _cCfgPtr->pidFilePath.c_str(),
                   strerror(errno) );
        exit(1);
    } else {
        storedPidFilePath = new std::string(_cCfgPtr->pidFilePath);
    }
    fprintf( pidFileDescriptor, "%d\n", _cCfgPtr->thisPID );
    fclose(pidFileDescriptor);
}

App::~App() {
    if( storedPidFilePath && unlink(_cCfgPtr->pidFilePath.c_str()) ) {
        nc_log_print( ncddi_critical,
                   "Couldn't delete PID file %s : %s.\n",
                   _cCfgPtr->pidFilePath.c_str(),
                   strerror(errno) );
    } else {
        if( storedPidFilePath ) { delete storedPidFilePath; }
    }
    close_log_file();
}

void
App::run() {
    sigset_t set, orig;
    siginfo_t siginfo;

    sigemptyset( &set );
    sigaddset( &set, SIGINT );
    sigaddset( &set, KICK_SIGNAL );
    if( sigprocmask(SIG_BLOCK, &set, &orig) ) {
        nc_eprintf( "Couldn't set procmask: %s.\n", strerror(errno) );
        exit(EXIT_FAILURE);
    }

    bzero( &siginfo, sizeof(siginfo) );

    int rc;
    while(1) {
        nc_dprintf( "Pending loop (re)start.\n" );
        // returns signal code or negative value on error.
        if( 0 > (rc = sigwaitinfo( &set, &siginfo )) ) {
            nc_eprintf( "sigwaitinfo() malfunction: %s.\n", strerror(errno) );
            exit(EXIT_FAILURE);
        }
        if( SIGINT == rc ) {
            nc_iprintf( "Got interrupting signal. Exiting pending loop.\n" );
            break;
        } else if( KICK_SIGNAL == rc ) {
            nc_dprintf( "Got KICK_SIGNAL. Gotta do something to do.\n" );
            this->_V_treat();
        }
    };
}

void
App::sighandler(int signum, siginfo_t *info, void *ptr) {
    nc_dprintf( "Recieved signal %d from %d.\n",
              signum, info->si_pid );
}

std::string
strfmt( const std::string & fmt_str, ... ) {
    int final_n, n = ((int)fmt_str.size()) * 2; /* reserve 2 times as much as the length of the fmt_str */
    std::string str;
    # if defined(__GXX_EXPERIMENTAL_CXX0X) || __cplusplus >= 201103L
    std::unique_ptr<char[]> formatted;
    va_list ap;
    while(1) {
        formatted.reset(new char[n]); /* wrap the plain char array into the unique_ptr */
        strcpy(&formatted[0], fmt_str.c_str());
        va_start(ap, fmt_str);
        final_n = vsnprintf(&formatted[0], n, fmt_str.c_str(), ap);
        va_end(ap);
        if (final_n < 0 || final_n >= n)
            n += abs(final_n - n + 1);
        else
            break;
    }
    return std::string(formatted.get());
    # else
    char * formatted;
    va_list ap;
    while(1) {
        formatted = new char[n]; /* wrap the plain char array into the unique_ptr */
        strcpy(formatted, fmt_str.c_str());
        va_start(ap, fmt_str);
        final_n = vsnprintf(formatted, n, fmt_str.c_str(), ap);
        va_end(ap);
        if (final_n < 0 || final_n >= n)
            n += abs(final_n - n + 1);
        else
            break;
    }
    std::string res(formatted);
    return res;
    # endif
}

}  // namespace ncddi

