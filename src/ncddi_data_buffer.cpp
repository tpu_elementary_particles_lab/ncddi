/* The MIT License (MIT)
 * 
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * */

# include <iostream>
# include "ncddi_data_buffer.hpp"

namespace ncddi {


/* Structure of data buffer:
 *  (s) -- status header (one byte)
 *     (c) -- content header (one byte)
 *        [rcode] -- return code, if content has nwrq_hasRCode -- sizeof(RCType)
 *           [appeal_host_id] -- host ID of device, if appeal provided -- sizeof(DevMinorID)
 *           [appeal] -- an 'Appeal' packed struct -- sizeof(struct Appeal)
 *             [var data size] -- length of var data, if has_binData enabled -- sizeof(Size)
 *             [var data ...] -- variadic length data, if has_binData enabled -- [var data size]
 *
 *  TODO: it is better to automate offsets calculation. Put these declaration
 *  in X-macro or smth...
 * */

Size
DataBuffer::init_for( UByte stat, UByte cont, Size varDataLen ) {
    Size len = 2;  // for `stat' and `cont' flags
    if( nwrq_hasRCode   & cont ) len += sizeof(RCType);
    if( nwrq_hasAppeal  & cont ) len += sizeof(struct Appeal) + sizeof(DevMinorID);
    if( nwrq_hasBinData & cont ) len += sizeof(Size) + varDataLen;
    this->reserve( len );

    *(this->data<UByte>(0)) = stat;
    *(this->data<UByte>(1)) = cont;

    *(this->data<Size>( 2 +
                (content_hdr() & nwrq_hasRCode  ? sizeof(RCType) : 0 ) +
                (content_hdr() & nwrq_hasAppeal ? sizeof(struct Appeal) + sizeof(DevMinorID) : 0 ) )) 
        = varDataLen;

    return len;
}

void
DataBuffer::str( std::ostream & os ) const {
    UByte stat = *(this->data<UByte>(0)),
          cont = *(this->data<UByte>(1));
    os << "Data buffer " << this << " of size " << reserved() << ":" << std::endl;
    os << "  - headers: 0x"
            << std::hex << (int) stat << std::dec << ", 0x" 
            << std::hex << (int) cont << std::dec << "." << std::endl;

    if( stat ) {
        os << "  - status: ";
        if( nwrq_rcMalform  & stat ) os << "malformed data, ";
        if( nwrq_rcError    & stat ) os << "describes error, ";
        if( nwrq_rcCheckIn  & stat ) os << "device check-in result, ";
        os << std::endl;
    }

    if( cont ) {
        os << "  - content: ";
        if( nwrq_hasRCode   & cont ) os << "return code, ";
        if( nwrq_hasAppeal  & cont ) os << "appeal, ";
        if( nwrq_hasBinData & cont ) os << "variadic length data, ";
        os << std::endl;
    }


    if( nwrq_hasRCode   & cont ) {
        os << "  - r-code: " << (int) rcode() << std::endl;
    }

    if( nwrq_hasBinData & cont ) {
        os << "  - bin data length: " << var_data_size() << std::endl;
    }

    if( nwrq_hasAppeal  & cont ) {
        os << "  - appeal type: " << (int) appeal().type << std::endl;
    }

}

}  // namespace ncddi

