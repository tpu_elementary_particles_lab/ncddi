Network Character Device Daemon I
=================================
A toolkit for sharing any character device via network.


What is NCDDI?
--------------

[NCDDI](https://bitbucket.org/CrankOne/ncddi) is a set of utils providing an ability to transmit operations with character devices within TCP protocol.

Its motivation is to provide an ability for amateur developers or small collectives to simplify using of their character devices drivers (kernel modules) when remote control needed. There is a typical situation when old hardware can be installed on ancient architecture only (ISA bus, unconvential protocols, etc.), but one need to use this devices on modern systems. Then NCDDI comes to the resque!


How it works?
-------------

In general, NCDDI provides `mimic` devices on client machine emulating some real devices on server (host) machines. The idea is that you can use `mimic` device in the same manner as you did it on your host, without rewriting your code. For simplest case we have a `host A` as a server (that hosts real device) and `host B` that is client we want to have device on. NCDDI has a kernel module for client host that populates mimic device(s), the client daemon that provides actual network datata transmissions, and the server daemon, that does the actual jobs with real character devices.


Installation
------------

Clone git-repo and make the out-of-source build directory. Note, that actual build directory should be called `debug` or `release`.

    $ git clone https://bitbucket.org/CrankOne/ncddi
    $ mkdir -p ncddi.build/release
    $ cd ncddi.build/release

The next line depends on for what machine NCDDI is built for. For client machine there are required the kernel module (called ncddi) and client application:

    $ cmake ../../ncddi -DCLIENT_APP=ON -DKERNEL_MODULE=ON

while for the server machine one need only the server daemon application:

    $ cmake ../../ncddi -DSERVER_APP=ON 

Now, you can actually build and install it:

    $ make
    # make install
    # depmod

For additional options, their explaination, and for viewing current build configuration, just append the cmake-command with -LH keys.


Usage
-----

When OpenRC system is emerged (you have an /etc/init.d directory):

    # /etc/init.d/ncddicd start

for client system, and

    # /etc/init.d/ncddisd start

for server system.

For non-rc systems (embedded, or ancient enough or whatever), you can use ncddicd/ncddisd binaries directly from build directory. Please, set the -c argument for them here.
])

Development
-----------

Any help will be appreciated. You can fork repo at bitbucket.org, or request for writing permissions if you have any contributions. Please, send any questions, patches, or notes. The similar project ([NCDD](http://sourceforge.net/projects/ncdd/) written by César Hernández Bañó) is more powerful but seems now to be abandoned. The only prominent lack of NCDD project is that its author uses his native language intensivily in C-sources, so that it is hard to understand and not possible to support further.


Bugs, shortcomings, etc.
------------------------

A development outline is located at TODO file (not in order of importance).


Copyright
---------

The module code is licensed under GPL v.2, the other significant contributions are distributed under MIT license.

Copyright (C) 2014 Q-Crypt Foundation, Renat R. Dusaev


