# The MIT License (MIT)
# 
# Copyright (C) 2014 Q-Crypt Foundation,
#                    Renat R. Dusaev
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

cmake_minimum_required( VERSION 2.6 )
project( ncddi )

set( NCDDI_VERSION_MAJOR 0 )
set( NCDDI_VERSION_MINOR 1 )

option(KERNEL_MODULE        "Build kernel module.")
option(CLIENT_APP           "Build client application.")
option(SERVER_APP           "Build server application.")
option(REUSABLE_SOCKET      "Insecure option for permissive port handling at server side (avoids blocking TCP port by incorrectly quenched server process)" ON)
option(START_STOP_DAEMON    "Install client/server rc-scripts into /etc/init.d" ON)
option(INTREPID_NETWORKING  "Disables sanity checks of data acquired by network. Enable it for trustworthy environment.")
option(ENABLE_GNUPP11       "Enables C++11 standard with GNU extensions support (should slightly increase performance of daemon apps).")
option(TESTING_APPS         "(DEVELOPMENT) Build a primitive testing applications.")
option(DEVELOPMENT_IOBUFFERS_DATA_VERBOSE "(DEVELOPMENT) On debug build, prints complete byte dump of all IO traffic transpassing via reentrant buffers")

SET( CMAKE_INSTALL_PREFIX "/usr" )

#
# Add custom uninstall target (CMake by default does not provide it)
CONFIGURE_FILE(
  "${CMAKE_CURRENT_SOURCE_DIR}/contrib/cmake_uninstall.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
  IMMEDIATE @ONLY)
ADD_CUSTOM_TARGET(uninstall
  "${CMAKE_COMMAND}" -P "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake") 

#
# Now ensure that it IS OUT-OF-SOURCE BUILD
function(AssureOutOfSourceBuilds)
    # make sure the user doesn't play dirty with symlinks
    get_filename_component(srcdir "${CMAKE_SOURCE_DIR}" REALPATH)
    get_filename_component(bindir "${CMAKE_BINARY_DIR}" REALPATH)
    # disallow in-source builds
    if(${srcdir} STREQUAL ${bindir})
        message("######################################################")
        message("You are attempting to build in your Source Directory.")
        message("You must run cmake from a build directory.")
        message("Note: couldn't remove CmakeFiles/ dir and CMakeCache.txt,")
        message("so you shold do it yourself,")
        message("######################################################")
        # attempt to remove cache and cache files... this actually fails to work,
        # but no hurt trying in case it starts working..
        file(REMOVE_RECURSE "${CMAKE_SOURCE_DIR}/CMakeCache.txt" "${CMAKE_SOURCE_DIR}/CMakeFiles")
        message(FATAL_ERROR "In-source build is forbidden.")
    endif()
    # check for polluted source tree
    if(EXISTS ${CMAKE_SOURCE_DIR}/CMakeCache.txt OR EXISTS ${CMAKE_SOURCE_DIR}/CMakeFiles)
        message("############################################################### #")
        message( "Found results from an in-source build in your source directory.")
        message("################################################################")
        # attempt to remove cache and cache files...
        file(REMOVE_RECURSE "${CMAKE_SOURCE_DIR}/CMakeCache.txt" "${CMAKE_SOURCE_DIR}/CMakeFiles")
        message(FATAL_ERROR "Source Directory Cleaned, please rerun CMake.")
    endif()
endfunction()

AssureOutOfSourceBuilds()

#
# Attain configuration depending on current build dir.
string(REPLACE "/" ";" SPLITTED_PATH ${CMAKE_BINARY_DIR})
list(REVERSE SPLITTED_PATH)
list(GET SPLITTED_PATH 0 buildDir)
message( STATUS "Building in directory ${buildDir}" )
if( buildDir STREQUAL "debug" )
    message( STATUS "NOTE: debug build" )
    set( CMAKE_BUILD_TYPE "Debug" )
    set( CMAKE_C_FLAGS   "${CMAKE_C_FLAGS} -g -fno-omit-frame-pointer" )  # -Wfatal-errors -fprofile-use? -fprofile-correction
    set( CMAKE_CXX_FLAGS   "${CMAKE_CXX_FLAGS} -g -fno-omit-frame-pointer" )
elseif( buildDir STREQUAL "release" )
    message( STATUS "NOTE: release build" )
    set( CMAKE_BUILD_TYPE "Release" )
    set( CMAKE_C_FLAGS   "${CMAKE_C_FLAGS} -DNDEBUG -O3" )
    set( CMAKE_CXX_FLAGS   "${CMAKE_CXX_FLAGS} -DNDEBUG -O3" )
else()
    message( STATUS "NOTE: custom build" )
    set( CMAKE_BUILD_TYPE "Release" )
    set( CMAKE_C_FLAGS   "${CMAKE_C_FLAGS} -DNDEBUG -O3" )
    set( CMAKE_CXX_FLAGS   "${CMAKE_CXX_FLAGS} -DNDEBUG -O3" )
endif()

#
# Configure source
set( NCDDI_LIB_INCLUDE_DIR "${PROJECT_SOURCE_DIR}/inc/" )
file(GLOB_RECURSE NCDDI_LIB_SOURCES src/*.c*)
configure_file (
    "${NCDDI_LIB_INCLUDE_DIR}/ncddi_cfg.h.in"
    "${NCDDI_LIB_INCLUDE_DIR}/ncddi_cfg.h"
)

#
# Configure compiler
set( CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -Wall -std=c99     -D_GNU_SOURCE -fexceptions -pthread -D_FILE_OFFSET_BITS=64 -rdynamic -DHAVE_INLINE -I${NCDDI_LIB_INCLUDE_DIR} -I${NCDDI_LIB_INCLUDE_DIR}/../contrib" )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -D_GNU_SOURCE -fexceptions -pthread -D_FILE_OFFSET_BITS=64 -rdynamic -I${NCDDI_LIB_INCLUDE_DIR} -I${NCDDI_LIB_INCLUDE_DIR}/../contrib" )
set( CMAKE_Haskell_FLAGS "-fPIC -no-hs-main" )

if( ENABLE_GNUPP11 )
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++11 " )
endif( ENABLE_GNUPP11 )

add_library( ncddiu SHARED ${NCDDI_LIB_SOURCES} )
install( TARGETS ncddiu LIBRARY DESTINATION lib )

if( CLIENT_APP )
    add_executable( ncddicd "utils/ncddi_client.cpp" )
    target_link_libraries( ncddicd ncddiu )
    install( TARGETS ncddicd RUNTIME DESTINATION bin )
    install(FILES assets/ncddicd.xml DESTINATION /etc COMPONENT config)
    if( START_STOP_DAEMON )
        install(PROGRAMS assets/ncddicd DESTINATION /etc/init.d COMPONENT config)
    endif( START_STOP_DAEMON )
endif( CLIENT_APP )

if( SERVER_APP )
   add_executable( ncddisd "utils/ncddi_server.cpp" )
   target_link_libraries( ncddisd ncddiu )
   install( TARGETS ncddisd RUNTIME DESTINATION bin )
   install(FILES assets/ncddisd.xml DESTINATION /etc COMPONENT config)
endif( SERVER_APP )

if( TESTING_APPS )
    add_executable( testIO "utils/client_side_test.cpp" )
    add_executable( testNW "utils/nw_test.cpp" )
    add_executable( testMod "utils/module_routines.c" "module/ncddi_buffer.c" )
    target_link_libraries( testIO ncddiu )
    target_link_libraries( testNW ncddiu )
    target_link_libraries( testMod ncddiu )
endif( TESTING_APPS )

if( KERNEL_MODULE )
   add_subdirectory( module ) 
endif( KERNEL_MODULE )

