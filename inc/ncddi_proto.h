/* The MIT License (MIT)
 * 
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * */

# ifndef H_NCDDI_NETWORK_PROTOCOL_H
# define H_NCDDI_NETWORK_PROTOCOL_H

# include "ncddi_cfg.h"

/**@!brief File containing data structures for IPC.
 * @!file ncddi_proto.h
 *
 * File contains data structures that applied
 * for IPC at NCDDI.
 *
 * Note: as far as using `packed' structures is
 * not a good idea in general, we should carefully
 * test all routines on all platforms that NCDDI
 * is provaded for.
 *
 * Each struct should have __attribute__((packed))
 * to prevent different field placement strategies
 * on different compilers. One should avoid usage of
 * any variadic-length types such as size_t, ssize_t,
 * etc.
 * */

/** In the C language a const or static const value is not
 * considered to be a "compile time constant", so we can not
 * initialize constant A with values of other constant, e.g. A = B|C,
 * where B,C are other compile time constants. So we should define
 * some extra stuff here only to make ANSI C compiler compute
 * codes automatically.
 */

# define mFlag_rqR  0x10
# define mFlag_rqW  0x20
# define mFlag_FIO  0x40
# define mFlag_IOC  0x80

# define for_each_appeal_direction_flag(X) \
    X( ef_requiresRead,     mFlag_rqR,          "requires var data reading" )               \
    X( ef_requiresWrite,    mFlag_rqW,          "requires var data writing" )               \
    /* ... */

# define for_each_appeal_instance_flag(X) \
    X( eg_FIO,              mFlag_FIO,          "file I/O operation" )                      \
    X( eg_ioctl,            mFlag_IOC,          "I/O control operation" )                   \
    /* ... */

# define for_each_appeal_full_descriptor_flag(X) \
    X( llseekOp,          0x1,                  "moves r/w file offset" )                   \
    X( openOp,            0x2,                  "opening" )                                 \
    X( releaseOp,         0x3,                  "descriptor releasing" )                    \
    X( readOp,    mFlag_FIO|mFlag_rqR,          "file reading" )                            \
    X( writeOp,   mFlag_FIO|mFlag_rqW,          "file writig" )                             \
    X( ioctlOp,           mFlag_IOC,            "generic I/O control" )                     \
    X( ioctlRO,   mFlag_IOC|mFlag_rqR,          "ioctl awaiting for a data")                \
    X( ioctlWO,   mFlag_IOC|mFlag_rqW,          "ioctl carrying a data" )                   \
    X( ioctlRW,   mFlag_IOC|mFlag_rqR|mFlag_rqW,"bidirectional ioctl" )                     \
    /* ... */

# define for_each_appeal_descriptive_constants(X)                                           \
    for_each_appeal_direction_flag(X)                                                       \
    for_each_appeal_instance_flag(X)                                                        \
    for_each_appeal_full_descriptor_flag(X)                                                 \
    /* ... */

# define declare_extern_constant( name, code, descr ) extern const uint8_t _nc_ ## name;
    for_each_appeal_descriptive_constants(declare_extern_constant)
# undef declare_extern_constant

enum AppealOpType {
    # define declare_enum_entry( name, code, descr ) e_ ## name = code,
        for_each_appeal_full_descriptor_flag( declare_enum_entry )
    # undef declare_enum_entry
};

/**@struct Appeal
 *
 * An 'appeal' abstraction that describes desired
 * operation/ioctl call invoked on mimic device.
 * Instances will be further sent to userspace
 */
struct __attribute__((packed)) Appeal {
    UByte type;  /**< What is this appeal actually.*/
    int16_t targetDeviceMinor;
    union __attribute__((packed)) OperationParameters {
        struct __attribute__((packed)) LlseekOp {
            /* submitted to device */
            int32_t offset;
            int16_t whence;
            /* comes from real device */
            int32_t res;
        } llseekOp;
        struct __attribute__((packed)) ReadOp {
            /* submitted to device */
            Size length;
            Size offset;
            /* comes from real device */
            BufferID rbfID;
            Size realBufLength;
        } readOp;
        struct __attribute__((packed)) WriteOp {
            /* submitted to device */
            Size bufferLength;
            BufferID rbfID;
            Size offset;
            /* comes from device */
            Size nWrote;
        } writeOp;
        struct __attribute__((packed)) OpenOp {
            /* submitted to device */
            uint16_t flags;
            /* comes from device: */
            int16_t res;
        } openOp;
        struct __attribute__((packed)) ReleaseOp { /* comes from device: */ int32_t res; } releaseOp;
        struct __attribute__((packed)) IoctlOp {
            /* submitted to device */
            BufferID rbfID;
            int32_t cmd;  /* should not be used at server,
                             except last two bytes */

            uint8_t deviceChar;  /* an device character (conventional) */
            uint8_t functionNum; /* 0-128 an device function (conventional) */

            uint32_t bfLength;  /* obtained by _IOC_SIZE just before
                                   dispatching to prevent mismatch
                                   between platforms */
            int64_t arg; /* not used by real device
                            when IO performed (will
                            be changed to r-buffer ptr), but
                            submitted anyway */
            /* comes from device */
            int64_t res;
        } ioctlOp;
        /* ... whatever? */
    } parsFor;
};


# ifdef __cplusplus
extern "C" {
# endif
/**@struct KMessage
 * A message structure implements data layout of simple
 * client diemon -to- module messages. This structures,
 * as a byte sequence (i.e. serialized) are submitted
 * cia root device from hub process (aka client daemon)
 * to require device creation or daemon quenching or
 * whatever.
 * */
typedef struct __attribute__((packed)) KMessage {
    enum MsgType {
        ncddi_addEntry                  =1, /* to add child mimic device to be imported */
        ncddi_daemonQuenching           =2, /* to cleanup child devices and set client pid to 0 */
        ncddi_appealDispatch            =3, /* to parse the rest of data as appeal */
        ncddi_appealDeliveringFailure   =4, /* appeal failed to deliver */
        /* ... */
    } msgType;
    union __attribute__((packed)) {
        /* add entry --------------- */
        struct __attribute__((packed)) {
            char name[DEVICE_NAMELEN];
            int permissions;  /* TODO: unsupported */
        } importDevice;
        struct __attribute__((packed)) {
            DevMinorID devno;
        } failureDetails;
        /* ... */
    } data;
} KMessage;

# define for_each_network_message_status_descriptor_flag(m)                         \
    m( 0x0,     rcOk,               "All fine." )                                   \
    m( 0x2,     rcMalform,          "response can be malformed")                    \
    m( 0x4,     rcError,            "error appeared during request treatment" )     \
    m( 0x8,     rcCheckIn,          "it is device check-in request" )               \
    /* ... */

# define for_each_network_message_content_descriptor_flag(m)                        \
    m( 0x1,     hasRCode,           "has integer code" )                            \
    m( 0x2,     hasAppeal,          "has appeal" )                                  \
    m( 0x4,     hasBinData,         "has binary data chunk of variadic length" )    \
    /* ... */

# define for_each_network_message_descriptor_flag(m)    \
    for_each_network_message_status_descriptor_flag(m)  \
    for_each_network_message_content_descriptor_flag(m)

# define declare_numerical_code( code, name, description ) \
extern const UByte nwrq_ ## name;
for_each_network_message_descriptor_flag( declare_numerical_code )
# undef declare_numerical_code

# ifdef __cplusplus
}
# endif

# ifdef __cplusplus
/**!@brief testing structure for ioctl calls.
 *
 * Convention of ioctl() arguments implies that actual data operation
 * provided within ioctl() call is encoded in cmd (second) argument
 * according to <kernel_src_dir>/Documentation/ioctl-decoding.txt.
 *
 * Following to this convention we suppose that any operation can be:
 *  a) Read-only operation, when argument should be interpreted as a
 *     buffer, where respondent data should be placed in.
 *  b) Write-only operation, when argument should be interpreted as a
 *     buffer, where data should be taken off and 'written' to actual
 *     device.
 *  c) Read-and-write operation, when argument should be interpreted as a
 *     mutable buffer, for reading data out and, further, data write.
 *  d) Operation, where any semantics should be obtained only from cmd
 *     and argument integer value.
 *
 * Taking into account the differences between actual data placement
 * in cmd, we shall decode command before sinding it to a server,
 * and encode it at server (see Appeal::parsFor.ioctlOp struct).
 *
 * The plot about this testing structure is that it is used as a buffer
 * at testing applications (see standalone/fake devices modes for client
 * and server applications).
 *
 * For details of command-building macros see:
 *      <kernel_src_dir>/Documentation/ioctl-decoding.txt
 *      /usr/include/asm-generic/ioctl.h
 **/
struct __attribute__((packed)) IOCTLTest {
    uint32_t clientAppealSize, serverAppealSize;
};

# endif /* __cplusplus */


# endif  /* H_NCDDI_NETWORK_PROTOCOL_H */
