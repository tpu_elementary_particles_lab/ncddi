/* The MIT License (MIT)
 * 
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * */

# ifndef H_NCDDI_APPLICATION_H
# define H_NCDDI_APPLICATION_H

# include <pthread.h>
# if defined(__GXX_EXPERIMENTAL_CXX0X) || __cplusplus >= 201103L
#   include <unordered_map>
# else 
#   include <map>
# endif
# include <queue>
# include <string>
# include <cassert>
# include <unistd.h>
# include <csignal>
# include "ncddi_logging.h"
# include "ncddi_proto.h"

namespace ncddi {

//
// Generic Thread Class
//

class Thread {
public:
    //static unsigned pendingTimeout; //TODO
private:
    /// An actual p-thread.
    pthread_t _thread;
    /// A threaded function (actual worker).
    static void *thread_func(void *d);
    /// A queue access synchronization mutex.
    mutable pthread_mutex_t tasksLock;
    /// A queue of tasks.
    std::queue<Appeal*> tasks;
protected:
    virtual void _V_run() = 0;
public:
    Thread();
    virtual ~Thread();
    virtual void run();
    int start();
    int wait ();
    /// (syncd) pushes appeal to the queue's back.
    void queue_push(Appeal *);
    /// (syncd) pops first element from queue.
    Appeal * queue_pull();
    /// (syncd) is the queue empty?
    bool is_queue_empty() const;
};  // class Thread


//
// Generic Application Class
//

class App {
public:
    static char cfgPath[256];
    struct CommonConfig {
        pid_t thisPID;
        std::string logFilePath;
        std::string pidFilePath;
        unsigned bufferLength;
        unsigned char nDevMax;
        // ...
    };
private:
    static App * _self;
    std::string * storedPidFilePath;    ///< to avoid automatic deletion.
protected:
    CommonConfig * _cCfgPtr;
    /// Application configure routine: uses cfgPath (virtual ctr part). Needs cCfg to be inited.
    virtual void _V_configure( bool asDaemon );
    /// Spawns a new thread of particular type.
    virtual Thread * _V_spawn_worker(DevMinorID) { assert(0); return NULL; };
    /// Raises the treatment operations.
    virtual void _V_treat() = 0;
    /// A dictionary of threads treating devices.
    //std::unordered_map<DevMinorID, Thread *> _devWDict;
    App() : storedPidFilePath(NULL), _cCfgPtr(NULL) { _self = this; }
public:
    App & self();
    virtual ~App();
    void configure( bool asDaemon=true );
    /// Spawns new worker.
    void spawn_worker( DevMinorID devMinorNumber );
    /// Adds new appeal to worker's queue.
    void queue_push( DevMinorID id, Appeal * appeal );
    /// Waits all workers to be done.
    void quench();

    /// Sets appropriate signal handlers and goes to pending cycle.
    virtual void run();

    /// Returns gneric config structure reference.
    inline const CommonConfig & generic_cfg() const { return *_cCfgPtr; }

    static void sighandler(int signum, siginfo_t *info, void *ptr);
};  // class Application

/// Uses printf()-like syntax to produce std::string in one-line expr.
std::string strfmt( const std::string & fmt, ... );

} // namespace ncddi

# endif // H_NCDDI_APPLICATION_H

