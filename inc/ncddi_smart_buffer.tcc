/* The MIT License (MIT)
 * 
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * */

# ifndef H_NCDDI_SMART_BUFFER_H
# define H_NCDDI_SMART_BUFFER_H

# include <cerrno>
# include <cstring>
# include <cstdio>
# include "ncddi_cfg.h"
# include "ncddi_logging.h"
# include "ncddi_app.hpp"

namespace ncddi {

/**!@brief Reentrant buffer class intended to storaging network
 *   requests data.
 * !@class SmartBuffer
 *
 * Depending on what memory block len is to be used, this class
 * provides fixed length data storage (usually, small), or
 * dynamically-allocated huge memory block.
 *
 * When ctr is invoked, instance allocates stack block of provided
 * length. This length is supposed to be sufficient for most of
 * data blocks used in routine.
 *
 * Routines that want to use this buffer, should submit first the
 * required block length via reserve() method. Depending on what
 * being 'reserved' Smart buffer may allocate new memory block of
 * length greater than reentrant length submitted to ctr. Or, if
 * clear() method wasn't invoked, and current length is lesser than
 * length of previous 'huge' storage length (nut still greater than
 * reentrant), instance can use previously allocated 'huge' data
 * storage.
 *
 * Guidlines:
 *  - Always reserve() required length before writing operations.
 *  - It is recommended to call clear() after a set of similar
 *    operations (not after each).
 *  - Do not rely on pointer returned by data() (do not store it
 *    between operation loops) as far as a lifetime of memory
 *    block depends on clear() invokations.
 */
template<unsigned short nStackReserved>
class SmartBuffer {
public:
    /// Length of reentrant buffer.
    static const Size reentrantLength;
private:
    /// Reentrant buffer: allocated once (heap too).
    char _reentrant[nStackReserved];

    Size   _hugeLen;
    char * _huge;    ///< Heap buffer. Allocated, if data length exceeds pre-allocated.

    Size _nReserved;
    bool _hugeUsed;
public:
    /// Ctr: allocates reentrant.
    SmartBuffer( );
    /// Dtr: frees all assotiated.
    ~SmartBuffer();
    /// Reserves memory block of required length.
    void reserve( Size n );
    /// Returns last reserve() length.
    inline Size reserved() const { return _nReserved; }
    /// Frees currently used heap memblock if needed.
    void clear();

    /// Uses ANSI C file descriptor to write size into.
    void write_size( int fd ) const;
    /// Uses ANSI C file descriptor to write data into.
    void write_data( int fd ) const;
    /// Uses ANSI C file descriptor to write size,data into.
    inline void write( int fd ) const;

    /// Uses ANSI C file descriptor to read size from.
    void read_size( int fd );
    /// Uses ANSI C file descriptor to read data from.
    void read_data( int fd );
    /// Uses ANSI C file descriptor to read size,data from.
    inline void read( int fd );

    /// Returns current data block beginn ptr.
    template<typename DT> DT * data( size_t n=0 ) {
        return reinterpret_cast<DT*>((_hugeUsed ? _huge : _reentrant)+n);
    }
    template<typename DT> const DT * data( size_t n=0 ) const {
        return reinterpret_cast<const DT*>((_hugeUsed ? _huge : _reentrant)+n);
    }
};  // class RequestData

//
// IMPLEMENTATION
//

# define TEMPLATE template<unsigned short nStackReserved>
# define CLASS SmartBuffer<nStackReserved>

TEMPLATE const Size CLASS::reentrantLength = nStackReserved;

TEMPLATE CLASS::SmartBuffer( ) :
    _hugeLen(false),
    _huge(_C11_nullptr),
    _hugeUsed(false) {}

TEMPLATE CLASS::~SmartBuffer( ) {
    // free heap buffer if needed.
    clear();
}

TEMPLATE void
CLASS::reserve( Size n ) {
    if( n > reentrantLength ) {  // can not use reentrant buf
        if( n > _hugeLen ) {  // can not use previously allocated buf
            clear();
            _hugeLen = n;
            if( _C11_nullptr == (_huge = new char[n]) ) {
                eraise( "Couldn't allocate %lu bytes for huge buffer.",
                        n );
            }
            _hugeUsed = true;
        }
    }
    _nReserved = n;
}

TEMPLATE void
CLASS::clear() {
    if( _C11_nullptr != _huge ) {
        delete [] _huge;
    }
    _huge = _C11_nullptr;
    _hugeUsed = false;
    _hugeLen = 0;
    _nReserved = reentrantLength;
}


TEMPLATE void
CLASS::write_size( int fd ) const {
    ssize_t nWritten;
    if( sizeof(Size) != (nWritten = ::write( fd, &_nReserved, sizeof(Size) )) ) {
        eraise( "Written %zd bytes of size descriptor instead of %lu: \"%s\"",
                 nWritten, _nReserved, strerror(errno));
    }
}

TEMPLATE void
CLASS::write_data( int fd ) const {
    ssize_t nWritten;
    if( ((ssize_t) _nReserved) != (nWritten = ::write( fd, data<char *>(), _nReserved ) ) ) {
        eraise( "Written %zd bytes of data instead of %lu: \"%s\"",
                 nWritten, _nReserved, strerror(errno));
    }
}

TEMPLATE void
CLASS::write( int fd ) const {
    write_size(fd);
    write_data(fd);
    nc_dprintf( "Written %lu bytes.\n", sizeof(Size) + (Size) _nReserved );  // XXX
}

// Note: read() return '0' when connection is closed by other side.

TEMPLATE void
CLASS::read_size( int fd ) {
    Size toRead;
    ssize_t nRead;
    if( sizeof(Size) != (nRead = ::read( fd, &toRead, sizeof(Size) )) ) {
        eraise( "Read %zd bytes instead of %zu: \"%s\")",
                nRead, sizeof(Size), strerror(errno) );
    }
    reserve( toRead );
}

TEMPLATE void
CLASS::read_data( int fd ) {
    ssize_t nRead;
    if( ((ssize_t) _nReserved) != (nRead = ::read( fd, data<char *>(), _nReserved )) ) {
        eraise( "Read %zd bytes instead of %lu): \"%s\"",
                nRead, _nReserved, strerror(errno) );
    }
}

TEMPLATE void
CLASS::read( int fd ) {
    read_size(fd);
    read_data(fd);
    nc_dprintf( "Read %lu bytes.\n", sizeof(Size) + (Size) _nReserved );  // XXX
}

# undef TEMPLATE
# undef CLASS

}  // namespace ncddi

# endif  // H_NCDDI_SMART_BUFFER_H

