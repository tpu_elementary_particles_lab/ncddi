/* The MIT License (MIT)
 * 
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * */

# ifndef H_NCDDI_NW_H
# define H_NCDDI_NW_H

/**@!brief A file containing all the network routines
 *  declarations for NCDDI.
 * @!file ncddi_nw.hpp
 *
 * All the network functionality of NCDDI can be splitted into
 * client/server roles. The server is passive while a client
 * always is a transmission initiator. These two classes wraps
 * ANSI C TCP/IP Berkley sockets implementation.
 */

# include <cstdio>
# include <cstdlib>
# include <unistd.h>
# include <cstring>
# include <sys/types.h>
# include <sys/socket.h>
# include <netinet/in.h>
# include <netdb.h> 
# include <string>
# include "ncddi_cfg.h"
# include "ncddi_data_buffer.hpp"

namespace ncddi {

/**!@brief A primitive AF_INET client instance.
 * !@class Client
 *
 * Once being associated
 * to particular 'server' host, become able to send a 'request' data
 * and wait for a response.
 *
 * Note: dispatch_request will allocate memory from heap with malloc().
 * To avoid memory leaks, user code MUST free() after all.
 * */
class Client {
private:
    bool _associated;
    int _sockfd;
protected:
    struct sockaddr_in _srvAddr;
    struct hostent * _server;
public:
    Client();
    virtual ~Client();

    /// Returns true only if this instance was associated at least once.
    inline bool is_associated() const { return _associated; }

    /// (Re)bounds this instance to particular server host.
    void associate( const std::string & serverHostname, int port );

    /// Unbounds this instance from any server (closes socket)
    void deassociate();

    /// Sends a 'request' data and hangs up until get a 'response' data. Note:
    /// outData will be allocated with malloc(), so one should free() it.
    void dispatch_request( const DataBuffer & in,
                           DataBuffer & out );
};  // class Client

/**!@brief A primitive AF_INET server instance.
 * !@class Server
 *
 * Being raised, creates
 * a listening queued socket and hangs until a 'request' data will be
 * acquired from any of distant 'clients'. Acquired data will be then
 * treated by _V_treat_request() PV-method and send back to the
 * awaiting client.
 * */
class Server {
private:
    int _sockfd;
    struct sockaddr_in _srvAddr,
                       _cliAddr;
protected:
    /// Treat an incoming request and prepare a feedback data.
    /// Should return 0 when job done (to close connection socket).
    /// Note: that is supposed that this routine internally uses malloc()
    /// to allocate outData array.
    virtual int _V_treat_request( const DataBuffer & in,
                                  DataBuffer & out ) = 0;
public:
    Server();
    virtual ~Server();
    int run( int portno );
};  // class Server

} // namespace ncddi

# endif // H_NCDDI_NW_H

