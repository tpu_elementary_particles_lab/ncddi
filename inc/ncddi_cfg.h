/* The MIT License (MIT)
 * 
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * */

/**@file ncddi_cfg.h.in
 *
 * A config file for ncddi utility.
 * Contains data structure definitions,
 * presets and generic macros that are
 * common for both, userspace utils and
 * kernel module.
 */

# ifndef H_NCDDI_CFG_H
# define H_NCDDI_CFG_H

# ifdef __cplusplus
# include <stdexcept>
# endif

# ifndef __cplusplus
#   ifndef __KERNEL__  /* The kernel always knows types. */
#       include <stdint.h>
#   else
#       include <linux/types.h>
#   endif
# else
#   if defined(__GXX_EXPERIMENTAL_CXX0X) || __cplusplus >= 201103L
#       include <cstdint>
        using std::int8_t;
        using std::uint8_t;
        using std::int16_t;
        using std::uint16_t;
        using std::int32_t;
        using std::uint32_t;
#   else /* No C++11 support */
# include <sys/types.h>
# if 1
# include <stdint.h>
# else
# include <limits.h>

# if UCHAR_MAX == 0xff
    //typedef char            int8_t;
    typedef unsigned char   uint8_t;
# else
    # error "Couldn't define 8-bit integer."
# endif

# if USHRT_MAX == 0xffff
    typedef short int       int16_t;
    typedef unsigned short  uint16_t;
# else
    # error "Couldn't define 16-bit integer."
# endif

# if ULONG_MAX == 0xffffffff
     typedef long            int32_t;
     typedef unsigned long   uint32_t;
# elif UINT_MAX == 0xffffffff
     typedef int             int32_t;
     typedef unsigned int    uint32_t;
# elif (UINT_MAX == 0xffffffffffffffff) && defined(__MTA__) 
      /* Integers are 64 bits on the MTA / XMT */
      typedef __int32           int32_t;
      typedef unsigned __int32  uint32_t; 
# else
    # error "Couldn't define 32-bit integer."
# endif

# if ULONG_MAX != 0xffffffff
#    if ULONG_MAX == 18446744073709551615u // 2**64 - 1
     typedef long                 int64_t;
     typedef unsigned long        uint64_t;
#    elif ULLONG_MAX == 18446744073709551615u // 2**64 - 1
     typedef long long                 int64_t;
     typedef unsigned long long        uint64_t;
#    else
#       error "Couldn't define 64-bit integer."
#    endif
# else /* assume no 64-bit integers */
#   error "Couldn't define 64-bit integer."
# endif
# endif /* 0 */

#   endif  /* No C++11 support */
# endif  /* Not C++ */

/*
 * Typedefs
 */
typedef int16_t Size;

/*
 * Common (both for userspace and kernel)
 */

typedef unsigned char UByte;
typedef UByte DevMajorID;
typedef UByte DevMinorID;
typedef uint16_t BufferID;
typedef int32_t RCType;

/** Device name / device name dir-prefix */
# define NCDDI_DEVICE_NAME "ncddi"
/** Maximal number devices to handle. */
# define MAX_DEVICES_AVAILIBLE 32
/** Root hub message buffer length */
# define ROOT_BUFFER_LENGTH 1024
/** Maximum length of single read/write operation */
# define MAX_ALLOWED_IO_BUF_LENGTH 64*1024*1024 /* 64 Mb */
/** Network buffer -- reentrant storage for socket IO. If exceed, heap is used. */
# define NETWORK_IO_BUFFER_LENGTH 1024 /* 1k per connection */
/** Kernel buffers -- reentrant storage for device IO. If exceed, kmalloc() is used. */
# define KERNEL_IO_BUFFER_LENGTH 124
/** Kernel buffers pool -- number of allowed reentrant buffer entries. */
# define KERNEL_IO_BUFFER_POOL_NUMBER 16
/** First message validity passphrase (compile-time arbitrary stuff) */
# define ROOT_PID_MSG_PASSPHRASE { 0xDE, 0xAD, 0xBE, 0xEF, 0x00 }
/** Max character number of raw device name (e.g. dsp, urandom, etc.)*/
# define DEVICE_NAMELEN 64
/** Signal to kick hub process with. */
# define KICK_SIGNAL SIGUSR2

# ifndef __KERNEL__
# ifndef NDEBUG
# define nc_dprintf( ... ) nc_log_print( ncddi_debug, __VA_ARGS__ )
# else
# define nc_dprintf( ... ) ((void)(0));
# endif
# define nc_eprintf( ... ) nc_log_print( ncddi_error, __VA_ARGS__ )
# define nc_iprintf( ... ) nc_log_print( ncddi_info,  __VA_ARGS__ )

# define eraise( ... ) while(1){throw std::runtime_error(strfmt( __VA_ARGS__ ));break;}

#define REUSABLE_SOCKET
/* #undef INTREPID_NETWORKING */
/* #undef DEVELOPMENT_IOBUFFERS_DATA_VERBOSE */

/*
 * Standard-specific features
 */

# ifdef __cplusplus
    #if defined(__GXX_EXPERIMENTAL_CXX0X) || __cplusplus >= 201103L
        # define _C11_override override
        # define _C11_nullptr  nullptr
    # else
        # define _C11_override /* unsupported */
        # define _C11_nullptr  NULL
    # endif
# endif

# define RIF( expr ) while(1) { if( expr ) {                \
    char errbf[256];                                        \
    snprintf(errbf, 256, "Error at \"" #expr "\":\"%s\".\n", \
            strerror(errno) );                              \
    eraise(errbf);                                          \
} break;};

# else  /* __KERNEL__ defined */

# ifndef NDEBUG
# define nc_dprintf( ... ) while(1) {      \
    char bf[128];                       \
    char msgBf[256];                    \
    snprintf(bf, 128, __VA_ARGS__ );    \
    snprintf(msgBf, 256, "NCDDI D: %s [%s:%d at %s]\n", bf, __FILE__, __LINE__, __PRETTY_FUNCTION__); \
    printk(msgBf);                      \
    break;                              \
};
# else
# define nc_dprintf( ... ) ((void)(0));
# endif

# define nc_eprintf( ... ) while(1) {      \
    char bf[128];                       \
    char msgBf[256];                    \
    snprintf(bf, 128, __VA_ARGS__ );    \
    snprintf(msgBf, 256, "NCDDI E: %s [%s:%d at %s]\n", bf, __FILE__, __LINE__, __PRETTY_FUNCTION__); \
    printk(KERN_WARNING "%s", msgBf);   \
    break;                              \
};
# define nc_iprintf( ... ) while(1) {      \
    char bf[128];                       \
    char msgBf[256];                    \
    snprintf(bf, 128, __VA_ARGS__ );    \
    snprintf(msgBf, 256, "NCDDI: %s\n", bf); \
    break;                              \
};

int snprintf(char *buf, size_t size, const char *fmt, ...);
int krn_bzero( void * ptr, size_t len );

# endif /* __KERNEL__ */

/* Silly stringification macro; usage: xstr(macro_parameter) */
#define XSTR(a) _NC_STR(a)
#define _NC_STR(a) #a

# endif /*H_NCDDI_CFG_H*/

