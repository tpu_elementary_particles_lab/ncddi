/* The MIT License (MIT)
 * 
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * */

# ifndef H_NCDDI_DATA_BUFFER_H
# define H_NCDDI_DATA_BUFFER_H

# include "ncddi_smart_buffer.tcc"

namespace ncddi {

class DataBuffer : public SmartBuffer<NETWORK_IO_BUFFER_LENGTH> {
public:
    /// Reserves buffer to hold described content and sets descriptors.
    Size init_for( UByte stat, UByte cont, Size varDataLen=0 );

    inline void rcode( RCType );
    inline RCType rcode() const;

    inline DevMinorID appeal_host_id() const;
    inline void appeal_host_id( DevMinorID );
    inline Appeal & appeal();
    inline const Appeal & appeal() const;

    inline const Size var_data_size() const;
    inline UByte * var_data();
    inline const UByte * var_data() const;

    inline UByte status_hdr()  const { return *(this->data<UByte>(0)); }
    inline UByte content_hdr() const { return *(this->data<UByte>(1)); }

    void str( std::ostream & os ) const;
};

# ifndef NDEBUG
# define ASSERT_HAS_FLAG( flag, txtDescr )                                  \
if(!( nwrq_ ## flag & *(this->data<UByte>(1)) )){                           \
    eraise( "Buffer %p wasn't supposed to contain " txtDescr "\n", this );  \
}
# else
((void)(0));
# endif

void
DataBuffer::rcode( RCType trc ) {
    ASSERT_HAS_FLAG( hasRCode, "result code" );
    *(this->data<RCType>(2)) = trc;
}

RCType
DataBuffer::rcode() const {
    ASSERT_HAS_FLAG( hasRCode, "result code" );
    return *(this->data<RCType>(2));
}

DevMinorID
DataBuffer::appeal_host_id() const {
    ASSERT_HAS_FLAG( hasAppeal, "device appeal struct" );
    return *(this->data<DevMinorID>( 2 +
                        (content_hdr() & nwrq_hasRCode ? sizeof(RCType) : 0 ) ));
}

void
DataBuffer::appeal_host_id( DevMinorID id ) {
    ASSERT_HAS_FLAG( hasAppeal, "device appeal struct" );
    *(this->data<DevMinorID>( 2 +
                        (content_hdr() & nwrq_hasRCode ? sizeof(RCType) : 0 ) )) = id;
}

Appeal &
DataBuffer::appeal() {
    ASSERT_HAS_FLAG( hasAppeal, "device appeal struct" );
    return *(this->data<Appeal>( 2 +
                        sizeof(DevMinorID) +
                        (content_hdr() & nwrq_hasRCode ? sizeof(RCType) : 0 ) ));
}

const Appeal &
DataBuffer::appeal() const {
    ASSERT_HAS_FLAG( hasAppeal, "device appeal struct" );
    return *(this->data<Appeal>( 2 +
                sizeof(DevMinorID) +
                (content_hdr() & nwrq_hasRCode ? sizeof(RCType) : 0 ) ));
}

const Size
DataBuffer::var_data_size() const {
    ASSERT_HAS_FLAG( hasBinData, "data of variadic length" );
    return *(this->data<Size>( 2 +
                (content_hdr() & nwrq_hasRCode  ? sizeof(RCType) : 0 ) +
                (content_hdr() & nwrq_hasAppeal ? sizeof(struct Appeal) + sizeof(DevMinorID) : 0 ) ));
}

UByte *
DataBuffer::var_data() {
    ASSERT_HAS_FLAG( hasBinData, "data of variadic length" );
    return (this->data<UByte>( 2 +
                (content_hdr() & nwrq_hasRCode  ? sizeof(RCType) : 0 ) +
                (content_hdr() & nwrq_hasAppeal ? sizeof(struct Appeal) + sizeof(DevMinorID) : 0 ) +
                sizeof(Size) ));
}

const UByte *
DataBuffer::var_data() const {
    ASSERT_HAS_FLAG( hasBinData, "data of variadic length" );
    return (this->data<UByte>( 2 +
                (content_hdr() & nwrq_hasRCode  ? sizeof(RCType) : 0 ) +
                (content_hdr() & nwrq_hasAppeal ? sizeof(struct Appeal) + sizeof(DevMinorID) : 0 ) +
                sizeof(Size) ));
}

# undef ASSERT_HAS_FLAG

}  // namespace ncddi

# endif  // H_NCDDI_DATA_BUFFER_H

