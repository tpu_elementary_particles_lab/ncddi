/* The MIT License (MIT)
 * 
 * Copyright (C) 2014 Q-Crypt Foundation,
 *                    Renat R. Dusaev
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * */

# ifndef H_NCDDI_LOGGING_H
# define H_NCDDI_LOGGING_H

# include "ncddi_cfg.h"

# ifdef __cplusplus
extern "C" {
# endif /*__cplusplus*/

enum ncddiMsgType {
    ncddi_debug     = 0,
    ncddi_info      = 1,
    ncddi_warning   = 2,
    ncddi_error     = 3,
    ncddi_critical  = 4,
};

/** Initializes logging stream. */
int open_log_file( const char * filename );

/** Returns -1 if log doesn't exists, or returns nc_log_print() rc. */
int close_log_file();

/** Puts message to logging stram */
int nc_log_print( UByte msgType, const char * fmt, ... );

# ifdef __cplusplus
}  /* extern "C" */
# endif /*__cplusplus*/

# endif /* H_NCDDI_LOGGING_H */

